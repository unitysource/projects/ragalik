<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.3" name="overworld" tilewidth="16" tileheight="16" spacing="1" tilecount="1767" columns="57">
 <image source="../../../Downloads/Kenney1/Spritesheet/roguelikeSheet_transparent.png" width="968" height="526"/>
 <terraintypes>
  <terrain name="New Terrain" tile="0"/>
  <terrain name="path" tile="0"/>
  <terrain name="water" tile="0"/>
  <terrain name="trees" tile="0"/>
  <terrain name="treesRed" tile="0"/>
 </terraintypes>
 <tile id="5" terrain="1,1,,"/>
 <tile id="63" terrain=",,1,1"/>
 <tile id="171" terrain="2,2,2,"/>
 <tile id="172" terrain="2,2,,2"/>
 <tile id="173" terrain=",,,2"/>
 <tile id="174" terrain=",,2,2"/>
 <tile id="175" terrain=",,2,"/>
 <tile id="228" terrain="2,,2,2"/>
 <tile id="229" terrain=",2,2,2"/>
 <tile id="230" terrain=",2,,2"/>
 <tile id="231" terrain="2,2,2,2"/>
 <tile id="232" terrain="2,,2,"/>
 <tile id="287" terrain=",2,,"/>
 <tile id="288" terrain="2,2,,"/>
 <tile id="289" terrain="2,,,"/>
 <tile id="635" terrain=",,0,0"/>
 <tile id="855" terrain="3,3,3,"/>
 <tile id="856" terrain="3,3,,3"/>
 <tile id="857" terrain=",,,3"/>
 <tile id="858" terrain=",,3,3"/>
 <tile id="859" terrain=",,3,"/>
 <tile id="912" terrain="3,,3,3"/>
 <tile id="913" terrain=",3,3,3"/>
 <tile id="914" terrain=",3,,3"/>
 <tile id="915" terrain="3,3,3,3"/>
 <tile id="916" terrain="3,,3,"/>
 <tile id="971" terrain=",3,,"/>
 <tile id="972" terrain="3,3,,"/>
 <tile id="973" terrain="3,,,"/>
 <tile id="1026" terrain="4,4,4,"/>
 <tile id="1027" terrain="4,4,,4"/>
 <tile id="1028" terrain=",,,4"/>
 <tile id="1029" terrain=",,4,4"/>
 <tile id="1030" terrain=",,4,"/>
 <tile id="1083" terrain="4,,4,4"/>
 <tile id="1084" terrain=",4,4,4"/>
 <tile id="1085" terrain=",4,,4"/>
 <tile id="1086" terrain="4,4,4,4"/>
 <tile id="1087" terrain="4,,4,"/>
 <tile id="1142" terrain=",4,,"/>
 <tile id="1143" terrain="4,4,,"/>
 <tile id="1144" terrain="4,,,"/>
</tileset>
