﻿using Mehanics.Objects.Systems;
using UnityEngine;

namespace Mehanics
{
    public class AliveObjects : MonoBehaviour
    {
        public ObjectState CurState { get; set; }
        
        public void ChangeState(ObjectState newState)
        {
            if (CurState != newState) CurState = newState;
        }
        
        public bool CompareState(ObjectState newState) =>
            CurState == newState;
    }
}