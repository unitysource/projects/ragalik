﻿using System;
using ScriptableObjects;
using UnityEngine;

namespace Mehanics.Objects
{
    public class InteractableDoor : Door
    {
        private Interactable _interactable;
        [SerializeField] private Inventory playerInventory;

        private bool _isActive;

        protected override void OnEnable()
        {
            base.OnEnable();
            _interactable = CurrDoorType switch
            {
                DoorType.Key => transform.GetComponentInChildren<Interactable>(),
                DoorType.Switch => transform.Find("Switch").GetComponentInChildren<Interactable>(),
                _ => _interactable
            };
            if (_isActive && CurrDoorType is DoorType.Key) OpenDoor();
        }

        private void Update()
        {
            if (!(Input.GetMouseButtonDown(0) && _interactable.IsPlayerStay)) return;
            if (CurrDoorType is DoorType.Key) KeyDoor();
            else if (CurrDoorType is DoorType.Switch)
                _interactable.transform.parent.GetComponent<Switch>().SwitchOn();
        }

        private void KeyDoor()
        {
            // Debug.Log(playerInventory.KeysNumber);
            if (playerInventory.KeysNumber == 0) return;
            playerInventory.KeysNumber--;
            OpenDoor();
            _isActive = true;
            PlayerPrefs.SetInt(StorageName, _isActive ? 1 : 0);

        }
    }
}