﻿// using System;
// using UnityEngine;
//
// namespace Mehanics
// {
//     [RequireComponent(typeof(Collider2D))]
//     public class DamageSystem : MonoBehaviour
//     {
//         [SerializeField] private float damage;
//         [SerializeField] private string otherTag;
//
//         private void OnTriggerEnter2D(Collider2D other)
//         {
//             if (!other.CompareTag(otherTag) || !other.isTrigger) return;
//             var healthSystem = other.GetComponent<HealthSystem>();
//             if(!healthSystem) return;
//             healthSystem.Damage(damage);
//         }
//     }
// }