﻿using System.Collections.Generic;
using Core;
using ScriptableObjects;
using UnityEngine;

namespace Mehanics.Objects.Systems
{
    public class WeaponSystem : MonoBehaviour
    {
        [SerializeField] [Range(1, 10)] private int capacity;
        [SerializeField] private CharacterPresets character;
        
        private List<Weapon> _weapons;
        private SpriteRenderer _weaponSpriteRenderer;

        public Weapon CurrentWeapon { get; private set; }

        private void Awake()
        {
            if(gameObject.CompareTag(Tags.Player))
                _weaponSpriteRenderer = transform.Find("Weapon").GetComponent<SpriteRenderer>();
            CurrentWeapon = character.ResetWeapon;
            Debug.Log(character.ResetWeapon.IsBroken);
            _weapons = new List<Weapon>(capacity) {CurrentWeapon};
        }

        public bool TryAddWeapon(Weapon weapon)
        {
            if (weapon is null || _weapons.Count >= capacity) return false;
            _weapons.Add(weapon);
            return true;
        }

        public void DropWeapon()
        {
            // _weapons.Remove(_currentWeapon);
            Debug.Log("drop weapon " + CurrentWeapon.Name);
        }

        public void SetWeapon(Weapon weapon) => 
            CurrentWeapon = weapon;

        public Weapon GetWeapon(int weaponIndex) => 
            _weapons.Count > weaponIndex ? _weapons[weaponIndex] : null;

        public void OnRotateWeapon(Vector2 direction)
        {
            if (direction.y > 0 && direction.x < 0)
            {
                _weaponSpriteRenderer.flipX = false;
                _weaponSpriteRenderer.sortingOrder = 0;
            }
            else if (direction.y > 0 && direction.x > 0)
            {
                _weaponSpriteRenderer.flipX = true;
                _weaponSpriteRenderer.sortingOrder = 4;
            }
            else if (direction.y < 0 && direction.x < 0)
            {
                _weaponSpriteRenderer.flipX = false;
                _weaponSpriteRenderer.sortingOrder = 4;
            }
            else if (direction.y < 0 && direction.x > 0)
            {
                _weaponSpriteRenderer.flipX = true;
                _weaponSpriteRenderer.sortingOrder = 0;
            }
        }
    }
}