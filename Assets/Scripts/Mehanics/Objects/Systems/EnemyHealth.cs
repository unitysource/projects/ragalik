﻿using System.Collections;
using ScriptableObjects;
using UnityEngine;

namespace Mehanics.Objects.Systems
{
    public class EnemyHealth : HealthSystem
    {
        public override void Damage(float damageAmount)
        {
            base.Damage(damageAmount);
            if (ObjectHealth.RuntimeValue > 0) return;
            if(TryGetComponent<LootSystem>(out var lootSystem))
                lootSystem.CreateLoot(); 
            if(TryGetComponent<DeathSystem>(out var deathSystem))
                deathSystem.Death();
            
            gameObject.SetActive(false);
        }
        
        // /// <summary>
        // /// Create a death effect when enemy is died
        // /// </summary>
        // private void EnemyDeath()
        // {
        //     if (deathPrefab is null) return;
        //     var deathEffect = Instantiate(deathPrefab, transform.position, Quaternion.identity);
        //     Destroy(deathEffect, deathEffectDelay);
        // }
    }
}