﻿using System;
using System.Collections;
using System.Linq;
using Mehanics.Objects.Systems;
using ScriptableObjects;
using UnityEngine;

namespace Mehanics
{
    [RequireComponent(typeof(DeathSystem))]
    public class HealthSystem : MonoBehaviour
    {
        [SerializeField] private FloatValue objectHealth;

        protected FloatValue ObjectHealth => objectHealth;
        private void Start()
        {
            objectHealth.RuntimeValue = objectHealth.InitialValue;
        }

        public void Knock(Rigidbody2D hitRb, float knockTime)
        {
            if (hitRb is null) return;
            StartCoroutine(KnockIe(hitRb, knockTime));
        }

        private IEnumerator KnockIe(Rigidbody2D hitRb, float knockTime)
        {
            if (gameObject.TryGetComponent<FlashSystem>(out var flashSystem))
                StartCoroutine(flashSystem.HitFlashIe());
            // playerHitSignal.Raise();

            yield return new WaitForSeconds(knockTime);
            hitRb.velocity = Vector2.zero;
            if (hitRb.CompareTag("enemy"))
                hitRb.GetComponent<EnemyModel>().ChangeState(ObjectState.Idle);
            else if (hitRb.CompareTag("Player"))
                hitRb.GetComponent<PlayerModel>().ChangeState(ObjectState.Idle);
        }

        public virtual void Heal(float healAmount) =>
            objectHealth.RuntimeValue =
                Mathf.Clamp(objectHealth.RuntimeValue += healAmount, 0, objectHealth.RuntimeValue);

        public void FullHeal() => objectHealth.RuntimeValue = objectHealth.RuntimeValue;

        public virtual void Damage(float damageAmount)
        {
            objectHealth.RuntimeValue =
                Mathf.Clamp(objectHealth.RuntimeValue -= damageAmount, 0, objectHealth.RuntimeValue);
        }

        public void InstantDeath() => objectHealth.RuntimeValue = 0;
    }
}