﻿using System.Collections;
using UnityEngine;

namespace Mehanics.Objects.Systems
{
    public class PlayerAttackBase : AttackSystem
    {
        private PlayerModel _model;
        
        //Delays
        private float _attackAnimDelay = .3f;
        private void Start() => 
            _model = GetComponent<PlayerModel>();

        public void OnWeaponAttack() => 
            StartCoroutine(OnWeaponAttackIe());    

        private IEnumerator OnWeaponAttackIe()
        {
            _model.ChangeState(ObjectState.Attack);
            yield return new WaitForSeconds(_attackAnimDelay);
            if (_model.CompareState(ObjectState.Interact)) yield break;
            _model.ChangeState(ObjectState.Walk);
        }
    }
}