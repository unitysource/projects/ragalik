﻿using System.Collections;
using ScriptableObjects;
using UnityEngine;

namespace Mehanics.Objects.Systems
{
    public class PlayerHealth : HealthSystem
    {
        [SerializeField] private Signal healthSignal;

        public override void Damage(float damageAmount)
        {
            CharacterPreset.ValueDelta = damageAmount;
            healthSignal.Raise();
        }
    }
}