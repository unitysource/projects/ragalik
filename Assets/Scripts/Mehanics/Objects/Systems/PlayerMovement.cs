using System;
using System.Collections;
using System.Linq;
using DG.Tweening;
using Mehanics.Objects;
using Mehanics.Objects.Systems;
using ScriptableObjects;
using UnityEngine;

namespace Mehanics
{
    public class PlayerMovement : Movement
    {
        
        /// <summary>
        /// when player moving. Only change position
        /// </summary>
        /// <param name="direction"></param>
        public void OmMovePlayer(Vector2 direction)
        {
            direction.Normalize();
            var position = transform.position;
            rb.MovePosition(position + new Vector3(direction.x, direction.y, position.z)
                * (Speed * Time.deltaTime));
        }
    }
}