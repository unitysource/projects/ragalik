﻿using System;
using System.Collections;
using UnityEngine;

namespace Mehanics
{
    public class FlashSystem : MonoBehaviour
    {
        [Header("IFrame Stuff")] 
        [SerializeField] private Color flashColor;
        [SerializeField] private Color regularColor;
        [SerializeField] private float oneFlashDuration;
        [SerializeField] private int flashesNumber;
        
        private Collider2D _collider;
        private SpriteRenderer _spriteRenderer;

        private void Start()
        {
            _spriteRenderer = GetComponentInParent<SpriteRenderer>();
            _collider = GetComponent<BoxCollider2D>();
        }

        public IEnumerator HitFlashIe()
        {
            var temp = 0;
            _collider.enabled = false;
            while (temp < flashesNumber)
            {
                _spriteRenderer.color = flashColor;
                yield return new WaitForSeconds(oneFlashDuration);
                _spriteRenderer.color = regularColor;
                yield return new WaitForSeconds(oneFlashDuration);
                temp++; 
            }
            _collider.enabled = true;
        }
    }
}