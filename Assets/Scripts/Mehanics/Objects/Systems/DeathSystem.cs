﻿using ScriptableObjects;
using UnityEngine;

namespace Mehanics.Objects.Systems
{
    [RequireComponent(typeof(HealthSystem))]
    public class DeathSystem : MonoBehaviour
    {
        [Tooltip("Prefab of death effect. it used when enemy is death")] 
        [SerializeField] private GameObject deathPrefab;
        [Header("Using only in death room")]
        [SerializeField] private Signal deathSignal;
        
        [Header("Death Animation Time")]
        [SerializeField] private float deathEffectDelay = .6f;
        
        /// <summary>
        /// Create a death effect when enemy is died
        /// </summary>
        public void Death()
        {
            if (deathPrefab is null) return;
            var deathEffect = Instantiate(deathPrefab, transform.position, Quaternion.identity);
            Destroy(deathEffect, deathEffectDelay);
            
            // It's for enemy rooms. Open door if I killed all enemies
            if(deathSignal != null)
                deathSignal.Raise();
        }
    }
}