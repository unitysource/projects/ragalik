﻿using Mehanics.Objects.Systems;
using ScriptableObjects;
using UnityEngine;

namespace Mehanics
{
    public class PlayerModel : AliveObjects
    {
        [SerializeField] private Inventory playerInventory;
        [SerializeField] private VectorValue startPosition;

        public Inventory PlayerInventory => playerInventory;

        private void Start()
        {
            ChangeState(ObjectState.Walk);
            transform.position = startPosition.InitialValue;
        }
    }
}