using System;
using Core;
using DG.Tweening;
using Mehanics;
using Mehanics.Objects.Systems;
using UnityEngine;

[RequireComponent(typeof(WeaponSystem))]
public class AttackSystem : MonoBehaviour
{
    [SerializeField] private bool haveHitBoxes;
    private WeaponSystem _weapon;

    private void Start()
    {
        _weapon = GetComponent<WeaponSystem>();
    }

    public void OnTriggerEnter2D(Collider2D other) => Attack(other, false);

    public void Attack(Collider2D other, bool fromHitBox)
    {
        if (!other.isTrigger) return;
        if (!other.gameObject.CompareTag(Tags.Enemy) ||
            !other.gameObject.CompareTag(Tags.Player) ||
            (haveHitBoxes && !fromHitBox)) return;
        var targetRb = other.GetComponent<Rigidbody2D>();
        
        
        _weapon.CurrentWeapon.WeaponDamage(targetRb, transform.position);

        // AddImpulse(hitRb);
        // var targetObject = other.GetComponent<AliveObjects>();
        // var targetHealth = other.GetComponent<HealthSystem>();
        // if (targetObject.CompareState(ObjectState.Stagger)) return;
        // targetObject.ChangeState(ObjectState.Stagger);
        // targetHealth.Knock(hitRb, knockTime);
        // targetHealth.Damage(-damage);
        // }
    }

    // private void AddImpulse(Rigidbody2D hitRb)
    // {
    //     if (hitRb is null) return;
    //     var position = hitRb.transform.position;
    //     var diff = (position - transform.position).normalized * thrust;
    //     hitRb.DOMove(position + diff, knockTime);
    // }

    // /// <summary>
    // /// Change PlayerStates and set the animation attack
    // /// </summary>
    // /// <returns></returns>
    // public IEnumerator AttackIe()
    // {
    //     anim.SetBool(Attack, true);
    //     PlayerModel.CurState = PlayerStates.Attack;
    //     yield return null;
    //     anim.SetBool(Attack, false);
    //     yield return new WaitForSeconds(.3f);
    //     if (PlayerModel.CurState == PlayerStates.Interact) yield break;
    //     PlayerModel.CurState = PlayerStates.Walk;
    // }
}