﻿using System;
using System.Xml.Serialization;
using UnityEngine;

namespace Mehanics
{
    public class PlayerAnimationsSystem : MonoBehaviour
    {
        private Animator _anim;
        private const float Delta = 0.1f;

        private static readonly int MoveX = Animator.StringToHash("moveX");
        private static readonly int MoveY = Animator.StringToHash("moveY");
        private static readonly int Move = Animator.StringToHash("move");
        private static readonly int Attack = Animator.StringToHash("attack");
        private static readonly int AttackTrigger = Animator.StringToHash("attackTrigger");
        private static readonly int ReceiveItem = Animator.StringToHash("receiveItem");

        private void Start()
        {
            _anim = GetComponent<Animator>();
            _anim.SetFloat(MoveX, 0);
            _anim.SetFloat(MoveY, -1);
        }

        /// <summary>
        /// Only when Player Stop movement
        /// </summary>
        public void PlayerIdleAnimation() =>
            _anim.SetBool(Move, false);


        /// <summary>
        /// when player moving. Only change animation
        /// </summary>
        /// <param name="direction"></param>
        public void UpdateMovementAnimation(Vector2 direction)
        {
            direction.x = direction.x > Delta
                ? Mathf.CeilToInt(direction.x)
                : direction.x < -Delta
                    ? Mathf.FloorToInt(direction.x)
                    : 0;
            direction.y = direction.y > Delta
                ? Mathf.CeilToInt(direction.y)
                : direction.y < -Delta
                    ? Mathf.FloorToInt(direction.y)
                    : 0;

            AnimationMovement(direction);
        }

        /// <summary>
        /// Flip the player and set in blind tree Move some values
        /// </summary>
        private void AnimationMovement(Vector2 direction)
        {
            _anim.SetFloat(MoveX, direction.x);
            _anim.SetFloat(MoveY, direction.y);
            _anim.SetBool(Move, true);
        }

        public void UpdateAttackAnimation()
        {
            _anim.SetTrigger(AttackTrigger);
            // _anim.SetBool(Attack, false);
        }

        public Vector2 ArrowVelocity() =>
            new Vector2(_anim.GetFloat(MoveX), _anim.GetFloat(MoveY));

        public Vector3 ArrowDirection() =>
            Vector3.forward * (Mathf.Atan2(_anim.GetFloat(MoveY), _anim.GetFloat(MoveX)) * Mathf.Rad2Deg - 90f);
    }
}