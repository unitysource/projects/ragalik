﻿using ScriptableObjects;
using UnityEngine;

namespace Mehanics.Objects.Systems
{
    public class LootSystem : MonoBehaviour
    {
        [SerializeField] private LootTable lootTable;

        public void CreateLoot()
        {
            // if this enemy have a loot
            if(lootTable == null) return;
            var newPowerUp = lootTable.LootPowerUp();
            // if chance worked
            if(newPowerUp is null) return;
            Instantiate(newPowerUp, transform.position, Quaternion.identity);
        }
    }
}