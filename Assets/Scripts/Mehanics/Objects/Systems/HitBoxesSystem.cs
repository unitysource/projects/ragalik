﻿using System;
using Core;
using DG.Tweening;
using UnityEngine;

namespace Mehanics.Objects.Systems
{
    public class HitBoxesSystem : MonoBehaviour
    {
        private AttackSystem _attackSystem;

        private void Start() =>
            _attackSystem = transform.root.GetComponent<AttackSystem>();

        private void OnTriggerEnter2D(Collider2D other)
        {
            // for breakableObject. Player have a several children cause we do this-transform.root.CompareTag("Player")
            // if (other.CompareTag("breakableObject") && transform.root.CompareTag("Player"))
            if (other.CompareTag(Tags.BreakableObject)) other.GetComponent<PotCrash>().Crash();

            _attackSystem.Attack(other, true);
        }
    }
}