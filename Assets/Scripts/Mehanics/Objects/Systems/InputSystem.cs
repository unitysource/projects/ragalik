﻿using System;
using ScriptableObjects;
using UnityEngine;
using UnityEngine.Events;

namespace Mehanics.Objects.Systems
{
    public class InputSystem : MonoBehaviour
    {
        [SerializeField] private Joystick moveJoystick;
        [SerializeField] private Joystick shootJoystick;

        [Header("Player Movement")] [SerializeField]
        private UnityEvent<Vector2> onMovement;
        [SerializeField] private UnityEvent onIdle;
        [Header("Player Long-range Attack")] [SerializeField]
        private UnityEvent<Vector2> onShoot;
        // [SerializeField] private UnityEvent onStopShoot;

        private const float JoystickTolerance = 0.001f;

        private bool _isMoving;

        private void Update()
        {
            if (Math.Abs(moveJoystick.Horizontal) > JoystickTolerance ||
                Math.Abs(moveJoystick.Vertical) > JoystickTolerance)
            {
                onMovement?.Invoke(moveJoystick.Direction);
                _isMoving = true;
            }
            else if (_isMoving)
            {
                onIdle?.Invoke();
                _isMoving = false;
            }
            
            if (Math.Abs(shootJoystick.Horizontal) > JoystickTolerance ||
                Math.Abs(shootJoystick.Vertical) > JoystickTolerance)
            {
                onShoot?.Invoke(shootJoystick.Direction);
                // _isMoving = true;
            }
            // else if (_isMoving)
            // {
            //     onIdle?.Invoke();
            //     // _isMoving = false;
            // }
        }
    }
}