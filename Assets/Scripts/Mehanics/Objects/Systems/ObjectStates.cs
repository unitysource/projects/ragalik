﻿namespace Mehanics.Objects.Systems
{
    public enum ObjectState
    {
        Attack,
        Walk,
        Stagger,
        Idle,
        Interact,
        Dead
    }
}