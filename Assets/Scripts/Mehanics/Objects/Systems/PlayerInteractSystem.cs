﻿using System;
using ScriptableObjects;
using UnityEngine;
using Mehanics;
using NUnit.Framework.Internal.Commands;

namespace Mehanics.Objects.Systems
{
    public class PlayerInteractSystem : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer receivedItemSprite;

        private PlayerModel _playerModel;

        private void Start()
        {
            _playerModel = GetComponent<PlayerModel>();
        }

        public void RaiseItem()
        {
            if (_playerModel.PlayerInventory.CurrItem is null) return;
            if (!_playerModel.CompareState(ObjectState.Interact))
            {
                // _anim.SetBool(ReceiveItem, true);
                _playerModel.ChangeState(ObjectState.Interact);
                receivedItemSprite.sprite = _playerModel.PlayerInventory.CurrItem.ItemSprite;
            }
            else
            {
                // _anim.SetBool(ReceiveItem, false);
                _playerModel.ChangeState(ObjectState.Idle);
                receivedItemSprite.sprite = null;
                _playerModel.PlayerInventory.CurrItem = null;
            }
        }
    }
}