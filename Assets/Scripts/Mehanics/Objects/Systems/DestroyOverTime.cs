﻿using System;
using System.Collections;
using System.Security.Cryptography;
using UnityEngine;

namespace Mehanics.Objects.Systems
{
    public class DestroyOverTime : MonoBehaviour
    {
        [Header("Life Time")] [SerializeField] private float lifeTime;

        private void Start() => StartCoroutine(DestroyIe());

        private IEnumerator DestroyIe()
        {
            yield return new WaitForSeconds(lifeTime);
            Destroy(gameObject, .01f);
        }
    }
}