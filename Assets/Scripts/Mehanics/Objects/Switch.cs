﻿using System;
using UnityEngine;

namespace Mehanics
{
    public class Switch : MonoBehaviour
    {
        [SerializeField] private Sprite activeSprite;

        private bool _isActive;
        private SpriteRenderer _spriteRenderer;
        private Door _door;

        private void Start()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _door = transform.parent.GetComponent<Door>(); // Door script in Door object
            // PlayerPrefs.SetInt(storageName, 0);
            _isActive = PlayerPrefs.GetInt(_door.StorageName) == 1;
            if(_isActive) SwitchOn();
        }
        
        /// <summary>
        /// It was creating for Button Door, cause Switch door doesn't have Trigger collider 
        /// </summary>
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!(other.CompareTag("Player") && !other.isTrigger &&
                  _door.CurrDoorType == DoorType.Button)) return;
            SwitchOn();
        }

        public void SwitchOn()
        {
            _isActive = true;
            PlayerPrefs.SetInt(_door.StorageName, _isActive ? 1 : 0);
            _door.OpenDoor();
            _spriteRenderer.sprite = activeSprite;
        }
    }
}