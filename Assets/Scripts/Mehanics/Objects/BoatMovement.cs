using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatMovement : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private Transform target;

    private bool _hasPlayer = false;
    private int _posInt;
    private GameObject _player;

    private Vector3 _startPos;

    void Start()
    {
        _startPos = transform.position;

        // if(GameObject.FindWithTag("Player") == null) return;
        _player = GameObject.FindWithTag("Player");

        _posInt = 0;
    }

    private void Update()
    {
        if (!_hasPlayer) return;

        var tarPos = target.transform.position;
        var pos = transform.position;

        
        if (transform.position != target.position && _posInt == 0)
        {
            transform.position = Vector3.MoveTowards(pos, tarPos, speed*Time.deltaTime);
            _player.transform.position = Vector3.MoveTowards(pos,
                new Vector3(tarPos.x, tarPos.y, tarPos.z),
                speed*Time.deltaTime);
        }
        else if(transform.position == target.position && _posInt == 1)
        {
            transform.position = Vector3.MoveTowards(tarPos, _startPos, speed*Time.deltaTime);
            _player.transform.position = Vector3.MoveTowards(tarPos,
                new Vector3(_startPos.x, _startPos.y, _startPos.z), speed*Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Player")) return;
        _hasPlayer = true;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Player")) return;
        _hasPlayer = false;
        _posInt = transform.position == target.transform.position ? 1 : 0;
    }

    // private IEnumerator TimeToChange()
    // {
    //     var predicate = Vector3.Distance(_player.transform.position, transform.position);
    //     yield return new WaitUntil(() => predicate >= minDistance);
    // }
}