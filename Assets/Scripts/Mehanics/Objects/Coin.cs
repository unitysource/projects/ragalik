﻿using ScriptableObjects;
using UnityEngine;

namespace Mehanics
{
    public class Coin : PowerUp
    {
        [SerializeField] private Inventory playerInventory;
        [SerializeField] private byte increaseAmount;

        private void Start()
        {
            powerUpSignal.Raise();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!(other.CompareTag("Player") && !other.isTrigger)) return;
            playerInventory.CoinsNumber += increaseAmount;
            powerUpSignal.Raise();
            Destroy(gameObject, .1f);
        }
    }
}