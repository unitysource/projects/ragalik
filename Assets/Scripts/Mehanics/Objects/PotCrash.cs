using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotCrash : MonoBehaviour
{
    private Animator _animator;
    private BoxCollider2D _coll;
    private static readonly int CrashAnim = Animator.StringToHash("crash");

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _coll = GetComponent<BoxCollider2D>();
    }

    /// <summary>
    /// Crash the pot
    /// </summary>
    public void Crash()
    {
        _animator.SetBool(CrashAnim, true);
        //So that player could go through this
        _coll.enabled = false;
        StartCoroutine(BreakIe(.7F));
    }

    private IEnumerator BreakIe(float animTime)
    {
        yield return new WaitForSeconds(animTime);
        _coll.enabled = true;
        gameObject.SetActive(false);
    }
}