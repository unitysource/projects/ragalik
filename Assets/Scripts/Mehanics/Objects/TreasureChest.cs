﻿using System;
using System.Collections;
using Mehanics.Objects;
using ScriptableObjects;
using UnityEngine;
using UnityEngine.UI;

namespace Mehanics
{
    public enum ChestStates
    {
        Close,
        Unlock,
        Open,
    }
    public class TreasureChest : Interactable
    {
        [Tooltip("PlayerPrefs watch on this string")]
        [SerializeField] private string storageString;
        [Header("ScriptableObjects")]
        [SerializeField] private Item contentItem;
        [SerializeField] private Signal receiveItemSignal;
        [SerializeField] private Inventory playerInventory;
        [Header("Dialog and chest Objects")]
        [SerializeField] private GameObject dialogBox;
        [SerializeField] private Text dialogText;
        [SerializeField] private Sprite openChestSprite;

        private ChestStates _currState;
        private Animator _dialogAnim;
        private SpriteRenderer _spriteRenderer;
        
        private static readonly int Trigger = Animator.StringToHash("Trigger");

        private void Start()
        {
            _dialogAnim = dialogBox.GetComponent<Animator>();
            _spriteRenderer = GetComponent<SpriteRenderer>();

            _currState = (ChestStates) PlayerPrefs.GetInt(storageString);
            if(_currState == ChestStates.Open)
                _spriteRenderer.sprite = openChestSprite;
        }

        private void Update()
        {
            if (!(Input.GetMouseButtonDown(0) && IsPlayerStay)) return;
            switch (_currState)
            {
                case ChestStates.Close:
                    OpenChest();
                    break;
                case ChestStates.Unlock:
                    StayWhenChestOpen();
                    break;
            }
        }

        private void OpenChest()
        {
            dialogBox.SetActive(true);
            dialogText.text = contentItem.ItemDescription;
            playerInventory.AddItem(contentItem);
            playerInventory.CurrItem = contentItem;
            // if(playerInventory.CurrItem == GlobalPrefabs.Bow) 
            receiveItemSignal.Raise();
            _currState = ChestStates.Unlock;
            Context.Raise();
            _spriteRenderer.sprite = openChestSprite;
            PlayerPrefs.SetInt(storageString, (int) ChestStates.Open);
        }

        private void StayWhenChestOpen()
        {
            receiveItemSignal.Raise();
            _currState = ChestStates.Open;
            StartCoroutine(AnimationIe(1f));
        }

        public override void OnTriggerEnter2D(Collider2D other)
        {
            if(_currState == ChestStates.Open) return;
            base.OnTriggerEnter2D(other);
        }

        public override void OnTriggerExit2D(Collider2D other)
        {
            if(_currState == ChestStates.Open) return;
            base.OnTriggerExit2D(other);
        }
        
        private IEnumerator AnimationIe(float time)
        {
            _dialogAnim.SetTrigger(Trigger);
            yield return new WaitForSeconds(time);
            dialogBox.SetActive(false);
        }
    }
}