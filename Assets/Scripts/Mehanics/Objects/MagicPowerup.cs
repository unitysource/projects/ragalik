﻿using System;
using ScriptableObjects;
using UnityEngine;

namespace Mehanics.Objects
{
    public class MagicPowerup : PowerUp
    {
        [SerializeField] private Inventory playerInventory;
        [Range(1f, 50f)] [SerializeField] private float increaseAmount = 30f;
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!(other.CompareTag("Player") && !other.isTrigger)) return;
            CharacterPreset.ValueDelta = increaseAmount;
            powerUpSignal.Raise();
            Destroy(gameObject, .1f);
        }
    }
}