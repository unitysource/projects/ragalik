﻿using System;
using Mehanics.Objects;
using ScriptableObjects;
using UnityEngine;

namespace Mehanics
{
    public enum DoorType
    {
        Enemy,
        Key,
        Button,
        Switch
    }

    public class Door : MonoBehaviour
    {
        [Header("Door variables")] [InspectorName("Door Type")] [SerializeField]
        private DoorType currDoorType;

        public DoorType CurrDoorType => currDoorType;

        [Tooltip("PlayerPrefs saved name based it")] [SerializeField]
        private string storageName;

        public string StorageName => storageName;
        private SpriteRenderer _doorSprite;
        private BoxCollider2D _collider;

        protected virtual void OnEnable()
        {
            _doorSprite = GetComponent<SpriteRenderer>();
            _collider = GetComponent<BoxCollider2D>();
        }

        public virtual void OpenDoor()
        {
            _doorSprite.enabled = false;
            _collider.enabled = false;
        }

        public void CloseDoor()
        {
            _doorSprite.enabled = true;
            _collider.enabled = true;
        }

        // public bool IsAbleToInteract() =>
        //     _isInteractableNotNull && Input.GetMouseButtonDown(0) && _interactable.IsPlayerStay;
    }
}