using Core;
using ScriptableObjects;
using UnityEngine;

namespace Mehanics.Objects
{
    public class Interactable : MonoBehaviour
    {
        public bool IsPlayerStay { get; private set; }
        [SerializeField] private Signal context;
        protected Signal Context => context;
    
        public virtual void OnTriggerEnter2D(Collider2D other)
        {
            if (!(other.CompareTag(Tags.Player) && !other.isTrigger)) return;
            IsPlayerStay = true;
            context.Raise();
        }
    
        public virtual void OnTriggerExit2D(Collider2D other)
        {
            if (!(other.CompareTag(Tags.Player) && !other.isTrigger)) return;
            IsPlayerStay = false;
            context.Raise();
        }
    }
}
