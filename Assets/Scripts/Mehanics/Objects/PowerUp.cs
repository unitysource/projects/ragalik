﻿using ScriptableObjects;
using UnityEngine;

namespace Mehanics
{
    public class PowerUp : MonoBehaviour
    {
        [SerializeField] protected Signal powerUpSignal;
    }
}