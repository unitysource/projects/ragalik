using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContextClue : MonoBehaviour
{
    [SerializeField] private GameObject tapText;

    public void ActiveChange() => 
        tapText.SetActive(!tapText.activeSelf);
}
