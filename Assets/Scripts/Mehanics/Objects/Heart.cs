﻿using System;
using ScriptableObjects;
using UnityEngine;

namespace Mehanics
{
    public class Heart : PowerUp
    {
        // [SerializeField] private FloatValue playerHealth;
        [SerializeField] private float increaseAmount;

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag("Player") || other.isTrigger) return;
            CharacterPreset.ValueDelta = increaseAmount;
            powerUpSignal.Raise();
            Destroy(gameObject, .1f);
        }
    }
}