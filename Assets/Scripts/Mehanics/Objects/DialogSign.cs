using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http.Headers;
using Core;
using Mehanics.Objects;
using UnityEngine;
using UnityEngine.UI;

public class DialogSign : Interactable
{
    [SerializeField] private GameObject dialogBox;
    [SerializeField] private Text dialogText;
    [SerializeField] private string dialog;

    private Animator _anim;
    private static readonly int Trigger = Animator.StringToHash("Trigger");

    private void Start() => 
        _anim = dialogBox.GetComponent<Animator>();

    protected virtual void Update()
    {
        if (!(Input.GetMouseButtonDown(0) && IsPlayerStay)) return;
        Debug.Log(IsPlayerStay);
        if (dialogBox.activeInHierarchy)
            StartCoroutine(AnimationIe(1f));
        else
        {
            dialogBox.SetActive(true);
            dialogText.text = dialog;
        }
    }

    public override void OnTriggerExit2D(Collider2D other)
    {
        base.OnTriggerExit2D(other);
        dialogBox.SetActive(false);
    }

    private IEnumerator AnimationIe(float time)
    {
        // if(Trigger != null)
        //ToDo Obj reference not set...
        _anim.SetTrigger(Trigger);
        yield return new WaitForSeconds(time);
        dialogBox.SetActive(false);
    }
}