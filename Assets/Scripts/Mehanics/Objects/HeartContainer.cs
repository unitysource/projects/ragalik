﻿using System;
using ScriptableObjects;
using UnityEngine;

namespace Mehanics.Objects
{
    public class HeartContainer : PowerUp
    {
        [SerializeField] private FloatValue playerHealth;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!(other.CompareTag("Player") && !other.isTrigger)) return;
            playerHealth.InitialValue += 1;
            // playerHealth.RuntimeValue += 1;
            powerUpSignal.Raise();
            Destroy(gameObject, .01f);
        }
    }
}