﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEditor;
using UnityEngine;

namespace Mehanics
{
    /// <summary>
    /// I should call folders in RoomObject correctly (Enemies, Pots)
    /// </summary>
    [RequireComponent(typeof(PolygonCollider2D))]
    public class Room : MonoBehaviour
    {
        [Header("Borders for polygon")] [SerializeField]
        private Vector2 minPosition;
        [SerializeField] private Vector2 maxPosition;
        
        [Header("Delay before doors will closing when player enter to enemy room")]
        [SerializeField] protected float playerEnterDelay = .3f;
        
        protected List<EnemyModel> Enemies;
        private List<PotCrash> _pots;
        private GameObject _virtualCamera;
        private PolygonCollider2D _polygon;

        protected virtual void Start()
        {
            _polygon = GetComponent<PolygonCollider2D>();

            _virtualCamera = transform.GetComponentInChildren<CinemachineVirtualCamera>().gameObject;
            _virtualCamera.SetActive(false);

            Enemies = new List<EnemyModel>();
            _pots = new List<PotCrash>();
            // I have two types of enemies. 1-EnemyModel component in parent and without children. 2-EnemyModel component in child
            foreach (Transform enemyObj in transform.Find("Enemies"))
                Enemies.Add(enemyObj.TryGetComponent<EnemyModel>(out var enemyModel)
                    ? enemyModel
                    : enemyObj.GetComponentInChildren<EnemyModel>());

            foreach (Transform potObj in transform.Find("Pots"))
                _pots.Add(potObj.GetComponent<PotCrash>());

            //ToDo Maybe it's not good
            foreach (var enemy in Enemies) ActiveChange(enemy, false);
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            if (!(other.CompareTag("Player") && !other.isTrigger)) return;
            StartCoroutine(PlayerEnterDelayIe());
        }

        protected void OnTriggerExit2D(Collider2D other)
        {
            if (!(other.CompareTag("Player") && !other.isTrigger)) return;
            //Deactivate all enemies and pots
            foreach (var enemy in Enemies) ActiveChange(enemy, false);
            foreach (var pot in _pots) ActiveChange(pot, false);
            // if(virtualCamera != null)
            _virtualCamera.SetActive(false);
        }
        
        protected virtual IEnumerator PlayerEnterDelayIe()
        {
            yield return new WaitForSeconds(playerEnterDelay);
            //Activate all enemies and pots
            foreach (var enemy in Enemies) ActiveChange(enemy, true);
            foreach (var pot in _pots) ActiveChange(pot, true);
            // if(virtualCamera != null) 
            _virtualCamera.SetActive(true);
        }

        private static void ActiveChange(Component component, bool isActivate) =>
            component.gameObject.SetActive(isActivate);

        public void SetPolygonBorders()
        {
            _polygon = GetComponent<PolygonCollider2D>();
            var points = new Vector2[4];

            points[0] = minPosition;
            points[1] = new Vector2(minPosition.x, maxPosition.y);
            points[2] = maxPosition;
            points[3] = new Vector2(maxPosition.x, minPosition.y);
            _polygon.points = points;
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(Room), true)]
    internal class RoomEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            GUILayout.Space(10);
            var room = (Room) target;
            if (GUILayout.Button("Set Polygon Borders"))
                room.SetPolygonBorders();
        }
    }
#endif
}