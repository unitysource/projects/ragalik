﻿using System;
using System.Linq;
using UnityEngine;

namespace Mehanics
{
    public class CameraBorders : MonoBehaviour
    {
        private CameraMovement _cameraMovement;

        private void Start()
        {
            _cameraMovement = GetComponent<CameraMovement>();
            //ToDo Delete me please
            PlayerPrefs.SetString("CameraStartPosition", "HouseToOverworld");
            SetDefaultBorders();
        }

        private void SetDefaultBorders()
        {
            Vector2 newMinPos;
            switch (PlayerPrefs.GetString("CameraStartPosition"))
            {
                case "DungeonToOverworld":
                    newMinPos = new Vector2(20, 0);
                    break;
                case "OverworldToDungeon":
                    newMinPos = new Vector2(-20, -40);
                    break;
                case "HouseToOverworld":
                    newMinPos = new Vector2(0, -20);
                    break;
                case "OverworldToHouse":
                    newMinPos = new Vector2(0, 0);
                    break;
                default:
                    throw new Exception("What the scene is it ???");
            }
            //ToDo Remove me
            newMinPos = new Vector2(20, -20);
            _cameraMovement.MinPos = newMinPos;
            _cameraMovement.DetectCameraBorders();
        }
    }
}