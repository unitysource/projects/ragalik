﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Mehanics
{
    public class DungeonEnemyRoom : DungeonRoom
    {
        private List<Door> _doors;

        protected override void Start()
        {
            base.Start();
            _doors = new List<Door>();
            foreach (Transform door in transform.Find("Doors"))
            {
                if (!door.TryGetComponent<Door>(out var doorClass)) return;
                if(doorClass.CurrDoorType != DoorType.Enemy) return;
                _doors.Add(doorClass);
            }

            //ToDo Maybe it's not good
            foreach (var door in _doors) door.OpenDoor();
        }

        private void OpenDoors()
        {
            foreach (var door in _doors) door.OpenDoor();
        }

        private void CloseDoors()
        {
            foreach (var door in _doors) door.CloseDoor();
        }

        public void CheckEnemies()
        {
            for (var i = 0; i < Enemies.Count; i++)
            {
                if (Enemies[i].gameObject.activeInHierarchy && i < Enemies.Count - 1) return;
                OpenDoors();
            }
        }

        protected override IEnumerator PlayerEnterDelayIe()
        {
            yield return StartCoroutine(base.PlayerEnterDelayIe());
            CloseDoors();
        }
    }
}