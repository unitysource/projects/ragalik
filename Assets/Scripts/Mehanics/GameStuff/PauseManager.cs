﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Mehanics
{
    public class PauseManager : MonoBehaviour
    {
        [SerializeField] private GameObject pausedPanel;
        [SerializeField] private Animator pauseBoxAnim;

        private const float ResumeAnimDelay = 1f;
        private bool _isPaused;
        private static readonly int Resume = Animator.StringToHash("Resume");


        private void Start()
        {
            pausedPanel.SetActive(false);
            _isPaused = false;
        }

        private void ChangePause()
        {
            _isPaused = !_isPaused;
            if (_isPaused)
            {
                pausedPanel.SetActive(true);
                Time.timeScale = 0f;
            }
            else
            {
                pausedPanel.SetActive(false);
                Time.timeScale = 1f;
            }
        }

        public void ResumeGame() => StartCoroutine(ResumeAnimIe());

        public void PauseGame() => ChangePause();

        public void QuitToMainMenu() => StartCoroutine(QuitIe());

        /// <summary>
        /// Coroutines doesn't work if timescale = 0 !!!
        /// </summary>
        /// <returns></returns>
        private IEnumerator ResumeAnimIe()
        {
            pauseBoxAnim.SetTrigger(Resume);
            Time.timeScale = 1;
            yield return new WaitForSeconds(ResumeAnimDelay);
            ChangePause();
        }

        private IEnumerator QuitIe()
        {
            pauseBoxAnim.SetTrigger(Resume);
            Time.timeScale = 1;
            yield return new WaitForSeconds(ResumeAnimDelay);
            var asyncOperation = SceneManager.LoadSceneAsync("StartMenuScene");
            while (!asyncOperation.isDone) yield return null;
        }
    }
}