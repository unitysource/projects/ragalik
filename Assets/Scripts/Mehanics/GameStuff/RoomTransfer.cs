using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Mehanics
{
    public class RoomTransfer : MonoBehaviour
    {
        [SerializeField] private Vector2 cameraChange;
        [SerializeField] private Vector2 playerChange;
        [Header("Title Settings")] 
        [SerializeField] private bool isNeedTitle = true;
        [SerializeField] private string title;
        [SerializeField] private float time = 3f;
        [SerializeField] private Text titleText;
        private Animator _animator;

        private CameraMovement _cameraMovement;
        private static readonly int Disappear = Animator.StringToHash("Disappear");

        private void Start()
        {
            _cameraMovement = GameObject.Find("CameraController").GetComponent<CameraMovement>();
            _animator = titleText.gameObject.GetComponent<Animator>();
            titleText.gameObject.SetActive(false);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!(other.CompareTag("Player") && !other.isTrigger)) return;
            _cameraMovement.MinPos += cameraChange;
            var otherTransform = other.transform;
            var position = otherTransform.position;
            position += new Vector3(playerChange.x, playerChange.y, position.z);
            otherTransform.position = position;
            // _cameraMovement.DetectCameraBorders();
            if (!isNeedTitle) return;
            StartCoroutine(TitleIe());
        }

        private IEnumerator TitleIe()
        {
            titleText.gameObject.SetActive(true);
            titleText.text = title;
            yield return new WaitForSeconds(time - 1);
            _animator.SetTrigger(Disappear);
            yield return new WaitForSeconds(1f);
            titleText.gameObject.SetActive(false);
        }
    }
}