using System;
using System.Collections;
using System.Collections.Generic;
using Mehanics;
using NUnit.Framework;
using ScriptableObjects;
using UnityEngine;
using UnityEngine.UI;

public class HeartManager : MonoBehaviour
{
    [SerializeField] private Sprite heart;
    [SerializeField] private FloatValue playerHealth;
    [SerializeField] private FloatValue maxHearts;
    [SerializeField] private GameObject heartsContainers;

    private Image[] _hearts;

    private void Start()
    {
        _hearts = new Image[(int) maxHearts.InitialValue];
        for (var i = 0; i < maxHearts.InitialValue; i++)
        {
            _hearts[i] = Instantiate(GlobalPrefabs.HeartImage, heartsContainers.transform);
            _hearts[i].enabled = false;
        }

        InitHearts();
    }

    private void InitHearts()
    {
        if (playerHealth.InitialValue > maxHearts.InitialValue)
        {
            playerHealth.InitialValue = maxHearts.InitialValue;
            return;
        }

        for (var i = 0; i < playerHealth.InitialValue; i++)
        {
            _hearts[i].enabled = true;
            _hearts[i].sprite = heart;
            if (i >= playerHealth.RuntimeValue) _hearts[i].color = Color.black;
        }
    }

    /// <summary>
    /// it work when player is knock
    /// </summary>
    public void UpdateHearts()
    {
        InitHearts();
        for (var i = 0; i < playerHealth.InitialValue; i++)
            _hearts[i].color = i >= playerHealth.RuntimeValue ? Color.black : Color.white;
    }
}