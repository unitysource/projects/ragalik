﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Mehanics
{
    public class MainMenuManager : MonoBehaviour
    {
        public void StartGame()
        {
            SceneManager.LoadScene("OverworldScene");
        }
        
        public void QuitGame()
        {
            Application.Quit();
        }
    }
}