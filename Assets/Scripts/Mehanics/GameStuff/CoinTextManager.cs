﻿using ScriptableObjects;
using TMPro;
using UnityEngine;

namespace Mehanics
{
    public class CoinTextManager : MonoBehaviour
    {
        [SerializeField] private Inventory playerInventory;
        [SerializeField] private TextMeshProUGUI coinText;

        public void UpdateCoinAmount() => 
            coinText.text = playerInventory.CoinsNumber.ToString();
    }
}