using System.Collections;
using UnityEngine;

namespace Mehanics
{
    public class CameraMovement : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private float smoothSpeed = 0.1f;
        
        [SerializeField] private Vector2 minPos;
        public Vector2 MinPos
        { get => minPos; set => minPos = value; }
        private Camera _camera;
        private Vector3 _offset;

        private Vector2 _minPos;
        private Vector2 _maxPos;
        private Animator _animator;
        private static readonly int Kick = Animator.StringToHash("screenKick");

        private void Start()
        {
            if (Camera.main != null) _camera = Camera.main;
            var cameraTrans = _camera.transform;
            _offset = Vector3.forward * cameraTrans.position.z;
            cameraTrans.position = target.position + _offset;
            // DetectCameraBorders();
            _animator = _camera.GetComponent<Animator>();
        }
        /// <summary>
        /// Find camera positions borders for any screen resolution
        /// </summary>
        public void DetectCameraBorders()
        {
            var cameraHalfHeight = _camera.orthographicSize; // half height
            var cameraHalfWidth = cameraHalfHeight * _camera.aspect; // half width
            _minPos = new Vector2(minPos.x + cameraHalfWidth, minPos.y + cameraHalfHeight);
            _maxPos = new Vector2(minPos.x+20f - cameraHalfWidth, minPos.y+20f - cameraHalfHeight);
            // Debug.Log(MinPos);
        }

        private void FixedUpdate()
        {
            if (_camera.transform.position == target.position) return;
            var targetPosition = target.position + _offset;

            targetPosition.x = Mathf.Clamp(targetPosition.x, _minPos.x, _maxPos.x);
            targetPosition.y = Mathf.Clamp(targetPosition.y, _minPos.y, _maxPos.y);

            var cameraPosition = Vector3.Lerp(_camera.transform.position, targetPosition, smoothSpeed);
            _camera.transform.position = cameraPosition;

            // if(Input.GetKey(KeyCode.Space))
            //     Debug.Log(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }

        public void ScreenKick()
        {
            _animator.SetBool(Kick, true);
            StartCoroutine(ScreenKickIe());
        }

        private IEnumerator ScreenKickIe()
        {
            yield return  new WaitForSeconds(.2f);
            _animator.SetBool(Kick, false);
        }
    }
}