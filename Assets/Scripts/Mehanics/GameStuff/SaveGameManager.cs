﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Mehanics
{
    public class SaveGameManager : MonoBehaviour
    {
        private static SaveGameManager _saveGame;
        [SerializeField] private List<ScriptableObject> objects = new List<ScriptableObject>();

        public static List<ScriptableObject> Objects { get; private set; }
        private void OnValidate() => Objects = objects;

        private void Awake()
        {
            if (_saveGame is null) _saveGame = this;
            else Destroy(gameObject);
            DontDestroyOnLoad(this);
        }

        private void OnEnable() => LoadObjects();

        private void OnDisable() => SaveObjects();

        private void SaveObjects()
        {
            for (int i = 0; i < objects.Count; i++)
            {
                var file = File.Create($"{Application.persistentDataPath}/{i}.data");
                var binary = new BinaryFormatter();
                var json = JsonUtility.ToJson(objects[i]);
                binary.Serialize(file, json);
                file.Close();
            }
        }

        public static void SaveObjectsStatic(List<ScriptableObject> newObjects)
        {
            for (int i = 0; i < newObjects.Count; i++)
            {
                var file = File.Create($"{Application.persistentDataPath}/{i}.data");
                var binary = new BinaryFormatter();
                var json = JsonUtility.ToJson(newObjects[i]);
                binary.Serialize(file, json);
                file.Close();
            }
        }

        private void LoadObjects()
        {
            for (int i = 0; i < objects.Count; i++)
            {
                if (!File.Exists($"{Application.persistentDataPath}/{i}.data")) return;
                var file = File.Open($"{Application.persistentDataPath}/{i}.data", FileMode.Open);
                var binary = new BinaryFormatter();
                JsonUtility.FromJsonOverwrite((string) binary.Deserialize(file), objects[i]);
                file.Close();
            }
        }

        public void ResetObjects()
        {
            for (int i = 0; i < objects.Count; i++)
            {
                if (!File.Exists($"{Application.persistentDataPath}/{i}.data")) return;
                File.Delete($"{Application.persistentDataPath}/{i}.data");
            }
        }
#if UNITY_EDITOR
        [CustomEditor(typeof(SaveGameManager), true)]
        internal class GameEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                DrawDefaultInspector();
                GUILayout.Space(10);
                var game = (SaveGameManager) target;
                GUILayout.Space(10);
                // GUILayout.Label("!!!Dangerous!!!");
                // if (GUILayout.Button("Reset all ScriptableObjects data")) 
                //     game.ResetObjects();

                GUILayout.Label("I make it in OnDisable, but why don't do it here?)");
                if (GUILayout.Button("Save all ScriptableObjects data"))
                    game.SaveObjects();
            }
        }
#endif
    }
}