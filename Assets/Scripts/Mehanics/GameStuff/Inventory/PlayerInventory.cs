﻿using System.Collections.Generic;
using ScriptableObjects;
using UnityEngine;

namespace Mehanics
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "New Inventory", menuName = "SCRIPTABLEOBJECTS/PLAYER INVENTORY/PLAYER INVENTORY")]
    public class PlayerInventory : ScriptableObject
    {
        [SerializeField] private List<InventoryItem> inventoryItems = new List<InventoryItem>();

        public List<InventoryItem> InventoryItems => inventoryItems;
        
    }
}