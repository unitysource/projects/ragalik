﻿using Core;
using ScriptableObjects;
using UnityEngine;

namespace Mehanics.InventoryItemReactions
{
    public class HealthReaction : MonoBehaviour
    {
        [SerializeField] private FloatValue playerHealth;
        [SerializeField] private Signal healthSignal;

        public void SetHealthReaction(int increaseAmount) =>
            HelpUtils.SetReaction(increaseAmount, playerHealth, healthSignal);
    }
}