﻿using Core;
using ScriptableObjects;
using UnityEngine;

namespace Mehanics.InventoryItemReactions
{
    public class MagicReaction : MonoBehaviour
    {
        [SerializeField] private FloatValue playerMagic;
        [SerializeField] private Signal magicSignal;

        //ToDo I don't have FloatValue playerMagic)
        public void SetMagicReaction(int increaseAmount) =>
            HelpUtils.SetReaction(increaseAmount, playerMagic, magicSignal);
    }
}