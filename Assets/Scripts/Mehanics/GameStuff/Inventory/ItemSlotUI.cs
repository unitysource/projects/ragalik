﻿using ScriptableObjects;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Mehanics
{
    [System.Serializable]
    public class ItemSlotUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI itemNumberText;
        [SerializeField] private Image itemImage;

        private InventoryItem _inventoryItem;
        private InventoryManager _inventoryManager;

        public void SetUp(InventoryItem newInvItem, InventoryManager newInvManager)
        {
            _inventoryItem = newInvItem;
            _inventoryManager = newInvManager;
            if (!_inventoryItem || !_inventoryManager) return;
            itemImage.sprite = _inventoryItem.ItemSprite;
            // itemImage.SetNativeSize();
            itemNumberText.text = _inventoryItem.NumberHeld.ToString();
        }

        /// <summary>
        /// It's working when I click on itemSlot
        /// </summary>
        public void ActivateDescriptionPlace() =>
            _inventoryManager.SetDescriptionPlace(_inventoryItem.ItemName, _inventoryItem.ItemDescription,
                _inventoryItem.Usable, _inventoryItem);
    }
}