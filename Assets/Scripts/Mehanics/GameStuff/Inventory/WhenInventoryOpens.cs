﻿using UnityEngine;

namespace Mehanics
{
    public class WhenInventoryOpens : MonoBehaviour
    {
        [SerializeField] private InventoryManager inventoryManager;

        private void OnEnable()
        {
            inventoryManager.ClearItemSlots();
            inventoryManager.CreateItemSlots();
            inventoryManager.SetDescriptionPlace(inventoryManager.DefaultName, inventoryManager.DefaultDescription,
                false, inventoryManager.InventoryItem);
        }
    }
}