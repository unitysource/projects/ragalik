﻿using ScriptableObjects;
using UnityEngine;

namespace Mehanics
{
    public class PhysicalInventoryItem : MonoBehaviour
    {
        [SerializeField] private InventoryItem inventoryItem;
        [SerializeField] private PlayerInventory generalPlayerInventory;

        private void AddItemToInventory()
        {
            if (!generalPlayerInventory || !inventoryItem) return;
            if (!generalPlayerInventory.InventoryItems.Contains(inventoryItem))
                generalPlayerInventory.InventoryItems.Add(inventoryItem);
            inventoryItem.NumberHeld++;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!(other.CompareTag("Player") && !other.isTrigger)) return;
            AddItemToInventory();
            Destroy(gameObject, .1f);
        }
    }
}