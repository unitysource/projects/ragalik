﻿using System;
using System.Collections;
using ScriptableObjects;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Mehanics
{
    public class InventoryManager : MonoBehaviour
    {
        [Header("Inventory Information")] [SerializeField]
        private GameObject itemSlot;

        [SerializeField] private GameObject itemSlotsBox;
        [SerializeField] private GameObject inventoryPanel;
        [SerializeField] private TextMeshProUGUI descriptionText;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private Toggle useToggle;
        [SerializeField] private string defaultName;

        public string DefaultName => defaultName;

        public string DefaultDescription => defaultDescription;
        [SerializeField] private string defaultDescription;

        [SerializeField] private PlayerInventory playerInventory;

        public InventoryItem InventoryItem { get; private set; }
        private bool _isInventoryOpen;
        private Animator _inventoryBoxAnimator;
        private static readonly int Close = Animator.StringToHash("close");
        
        private void Start()
        {
            _inventoryBoxAnimator = inventoryPanel.transform.GetChild(0).GetComponent<Animator>();
            inventoryPanel.SetActive(false);
            _isInventoryOpen = false;
            useToggle.isOn = false;
        }

        public void SetDescriptionPlace(string newName, string description, bool isToggleActive, InventoryItem newItem)
        {
            useToggle.isOn = false;
            useToggle.interactable = true;
            InventoryItem = newItem;
            nameText.text = newName;
            descriptionText.text = description;
            GameObject toggle;
            (toggle = useToggle.gameObject).SetActive(isToggleActive);
            descriptionText.GetComponent<RectTransform>().sizeDelta =
                toggle.activeSelf ? new Vector2(370f, 50f) : new Vector2(535f, 50f);
        }

        /// <summary>
        /// It's working when we click on toggle or if I change toggle.isOn property
        /// </summary>
        public void OnToggleClick()
        {
            if (!InventoryItem || !useToggle.isOn) return;
            useToggle.interactable = false;
            InventoryItem.UseToggle();
            // Clearing all slots of inventory
            ClearItemSlots();
            //Refill all slots using new numbers of count
            CreateItemSlots();
            //Set default name and descriptions
            if(InventoryItem.NumberHeld > 0) return;
            SetDescriptionPlace(defaultName, defaultDescription, false, InventoryItem);
        }
        
        public void CreateItemSlots()
        {
            if (!playerInventory) return;
            foreach (var item in playerInventory.InventoryItems)
            {
                if (item.NumberHeld <= 0 || item.ItemName is "Bottle") return;
                var newSlotGameObject = Instantiate(itemSlot, itemSlotsBox.transform.position, Quaternion.identity);
                newSlotGameObject.transform.SetParent(itemSlotsBox.transform, false);
                var newSlot = newSlotGameObject.GetComponent<ItemSlotUI>();
                if (!newSlot) return;
                newSlot.SetUp(item, this);
            }
        }
        
        public void ClearItemSlots()
        {
            for (int i = 0; i < itemSlotsBox.transform.childCount; i++)
                Destroy(itemSlotsBox.transform.GetChild(i).gameObject);
        }

        private void SetActiveInventory()
        {
            inventoryPanel.SetActive(!inventoryPanel.activeSelf);
            Time.timeScale = inventoryPanel.activeSelf ? 0 : 1;
        }
        
        public void CloseInventory()
        {
            Time.timeScale = 1;
            StartCoroutine(CloseAnimIe());
        }

        public void OpenInventory() => SetActiveInventory();
        
        private IEnumerator CloseAnimIe()
        {
            _inventoryBoxAnimator.SetTrigger(Close);
            yield return new WaitForSeconds(1f);
            SetActiveInventory();
        }
    }
}