﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ScriptableObjects;
using UnityEngine;

namespace Mehanics
{
    public class InventorySaver : MonoBehaviour
    {
        [SerializeField] private PlayerInventory generalPlayerInventory;

        // private void Awake()
        // {
        //     if (_saveGame is null) _saveGame = this;
        //     else Destroy(gameObject);
        //     DontDestroyOnLoad(this);
        // }

        private void OnEnable()
        {
            generalPlayerInventory.InventoryItems.Clear();
            LoadObjects();
        }

        private void OnDisable() => SaveObjects();

        private void SaveObjects()
        {
            ResetObjects();
            for (int i = 0; i < generalPlayerInventory.InventoryItems.Count; i++)
            {
                var file = File.Create($"{Application.persistentDataPath}/{i}.indv");
                var binary = new BinaryFormatter();
                var json = JsonUtility.ToJson(generalPlayerInventory.InventoryItems[i]);
                binary.Serialize(file, json);
                file.Close();
            }
        }

        private void LoadObjects()
        {
            int i = 0;
            while (File.Exists($"{Application.persistentDataPath}/{i}.indv"))
            {
                var tempItem = ScriptableObject.CreateInstance<InventoryItem>();
                var file = File.Open($"{Application.persistentDataPath}/{i}.indv", FileMode.Open);
                var binary = new BinaryFormatter();
                JsonUtility.FromJsonOverwrite((string) binary.Deserialize(file), tempItem);
                // Debug.Log(Application.persistentDataPath);
                file.Close();
                generalPlayerInventory.InventoryItems.Add(tempItem);
                i++;
            }
        }

        private void ResetObjects()
        {
            var i = 0;
            while (File.Exists($"{Application.persistentDataPath}/{i}.indv"))
            {
                File.Delete($"{Application.persistentDataPath}/{i}.indv");
                i++;
            }
        }
    }
}