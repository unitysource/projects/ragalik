using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ScriptableObjects;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransfer : MonoBehaviour
{
    [Header("New scene variables")] [SerializeField]
    private string loadScene;

    [SerializeField] private Vector2 playerPos;
    [SerializeField] private VectorValue positionStorage;

    [Header("Effects when change scene")] [SerializeField]
    private GameObject enableSceneEffect;

    [SerializeField] private GameObject disableSceneEffect;

    private void Awake()
    {
        if (enableSceneEffect is null) return;
        var img = Instantiate(enableSceneEffect, Vector3.zero, Quaternion.identity);
        Destroy(img, 1f);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!(other.CompareTag("Player") && !other.isTrigger)) return;
        positionStorage.InitialValue = playerPos;

        PlayerPrefs.SetString("CameraStartPosition", CreateTransitionString());

        StartCoroutine(DisableSceneIe(.5f));
    }

    /// <summary>
    /// Set the effect of scene transition and load new scene
    /// </summary>
    private IEnumerator DisableSceneIe(float animTime)
    {
        if (disableSceneEffect is null) yield break;
        Instantiate(disableSceneEffect, Vector3.zero, Quaternion.identity);
        yield return new WaitForSeconds(animTime);
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(loadScene);
        while (!asyncOperation.isDone) yield return null;
    }

    private string CreateTransitionString() =>
        $"{SceneManager.GetActiveScene().name.Split('S').First()}" +
        $"To{loadScene.Split('S').First()}";
}