﻿using System;
using ScriptableObjects;
using UnityEngine;
using UnityEngine.UI;

namespace Mehanics
{
    public static class GlobalPrefabs
    {
        public static Item Bow { get; internal set; }
        public static Item Key { get; internal set; }
        public static GameObject Coin { get; internal set; }
        public static GameObject Heart { get; internal set; }
        public static GameObject MagicBottle { get; internal set; }
        public static Image HeartImage { get; internal set; }
    }

    public class PrefabsModel : MonoBehaviour
    {
        [Header("Items")] [SerializeField] private Item bow;
        [SerializeField] private Item key;

        [Header("Power ups")] [SerializeField] private GameObject coin;
        [SerializeField] private GameObject heart;
        [SerializeField] private GameObject magicBottle;

        [Header("Images")]
        [SerializeField] private Image heartImage; 

        private void Awake()
        {
            GlobalPrefabs.Bow = bow;
            GlobalPrefabs.Key = key;
            GlobalPrefabs.Coin = coin;
            GlobalPrefabs.Heart = heart;
            GlobalPrefabs.MagicBottle = magicBottle;
            GlobalPrefabs.HeartImage = heartImage;
        }
    }
}