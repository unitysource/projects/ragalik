﻿using System;
using UnityEngine;

namespace Mehanics
{
    public class Movement : MonoBehaviour
    {
        [SerializeField] private float speed = 1f;

        public float Speed => speed;
        [HideInInspector] public Rigidbody2D rb;

        private void Start() => rb = GetComponent<Rigidbody2D>();

        public void Motion(Vector2 velocity) => 
            rb.velocity = velocity.normalized * speed;
    }
}