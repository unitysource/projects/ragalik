﻿using UnityEngine;

namespace Mehanics
{
    public class BulletMovement : MonoBehaviour
    {
        [Header("Bullet Properties")] [SerializeField]
        private float moveSpeed;

        [SerializeField] private Rigidbody2D rb;

        private void Start() => rb = GetComponent<Rigidbody2D>();

        public void Launch(Vector2 initialVelocity) =>
            rb.velocity = initialVelocity * moveSpeed;
    }
}