﻿using System;
using System.Collections;
using Mehanics.Objects;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Mehanics.NPC
{
    public class BoundedNPC : DialogSign
    {
        [SerializeField] private float speed = 2f;
        [SerializeField] private float minWaitTime = 0.2f;
        [SerializeField] private float maxWaitTime = 1.2f;
        [SerializeField] private float minMoveTime = 2f;
        [SerializeField] private float maxMoveTime = 4f;
        [SerializeField] private BoxCollider2D boundsCollider;

        private Vector3 _direction;
        private bool _isMoving;
        private float _moveTime;
        private float _waitTime;
        private Rigidbody2D _rb;
        private Animator _anim;
        private SpriteRenderer _spriteRenderer;
        private static readonly int MoveX = Animator.StringToHash("moveX");
        private static readonly int MoveY = Animator.StringToHash("moveY");

        private void Start()
        {
            _rb = GetComponent<Rigidbody2D>();
            _anim = GetComponent<Animator>();
            _spriteRenderer = GetComponent<SpriteRenderer>();

            _moveTime = Random.Range(minMoveTime, maxMoveTime);
            _waitTime = Random.Range(minWaitTime, maxWaitTime);

            ChangeDirection();
        }

        protected override void Update()
        {
            base.Update();
            if (_isMoving)
            {
                StartCoroutine(MoveOrWaitTimeIe(_moveTime));

                if (IsPlayerStay) return;
                MoveNpc();
            }
            else
                StartCoroutine(MoveOrWaitTimeIe(_waitTime));
        }

        private IEnumerator MoveOrWaitTimeIe(float time)
        {
            yield return new WaitForSeconds(time);
            _isMoving = !_isMoving;
            // if(isMoving)
            // ChooseNewDirection();
            if (Math.Abs(time - _moveTime) < 0.001f)
                _moveTime = Random.Range(minMoveTime, maxMoveTime);
            else
                _waitTime = Random.Range(minWaitTime, maxWaitTime);
        }

        private void ChangeDirection()
        {
            int dir = Random.Range(0, 4);
            switch (dir)
            {
                case 0:
                    _direction = Vector3.right;
                    break;
                case 1:
                    _direction = Vector3.left;
                    break;
                case 2:
                    _direction = Vector3.up;
                    break;
                case 3:
                    _direction = Vector3.down;
                    break;
                default:
                    throw new Exception("OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOu mai");
                    break;
            }

            AnimNpc();
        }

        private void MoveNpc()
        {
            var temp = transform.position + _direction * (speed * Time.deltaTime);
            if (boundsCollider.bounds.Contains(temp))
                _rb.MovePosition(temp);
            else ChangeDirection();
        }

        private void AnimNpc()
        {
            _anim.SetFloat(MoveX, _direction.x);
            _anim.SetFloat(MoveY, _direction.y);
            _spriteRenderer.flipX = _direction.x < 0;
        }

        public override void OnTriggerEnter2D(Collider2D other)
        {
            base.OnTriggerEnter2D(other);
            ChooseNewDirection();
        }

        private void ChooseNewDirection()
        {
            var temp = _direction;
            ChangeDirection();
            var loop = 0;
            while (temp == _direction && loop < 100)
            {
                loop++;
                ChangeDirection();
            }
        }
    }
}