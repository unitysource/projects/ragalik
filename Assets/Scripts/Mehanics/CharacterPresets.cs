﻿using ScriptableObjects;
using UnityEngine;

namespace Mehanics
{
    [CreateAssetMenu(fileName = "New CharacterPreset", menuName = "SCRIPTABLEOBJECTS/CharacterPreset")]
    public class CharacterPresets : ScriptableObject
    {
        [SerializeField] private string characterName;
        [SerializeField] private Weapon resetWeapon;
        [SerializeField] private CharacterPreset healthPreset;
        [SerializeField] private CharacterPreset patronsPreset;
        [SerializeField] private CharacterPreset shieldPreset;

        public Weapon ResetWeapon => resetWeapon;
        public CharacterPreset HealthPreset => healthPreset;
        public CharacterPreset PatronsPreset => patronsPreset;
        public CharacterPreset ShieldPreset => shieldPreset;
    }
}