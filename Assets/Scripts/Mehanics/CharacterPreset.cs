﻿using System;
using Core;
using UnityEngine;
using UnityEngine.UI;

namespace Mehanics
{
    [Serializable]
    public class CharacterPreset
    {
        public static float ValueDelta;
        
        public float NormalizeValue => CurrentValue / resetValue;
        [SerializeField] private float resetValue;
        public float BaseValue => resetValue;
        public float CurrentValue { get; private set; }

        public void UpdateValue() => 
            CurrentValue = Mathf.Clamp(CurrentValue + ValueDelta, 0, resetValue);

        public void ResetValue() => CurrentValue = resetValue;

        public bool CanDecrease() => Math.Abs(CurrentValue) > 0f;
    }
}