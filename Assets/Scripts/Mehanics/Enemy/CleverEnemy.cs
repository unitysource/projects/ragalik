﻿using System.Collections;
using Mehanics.Objects.Systems;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Mehanics
{
    public class CleverEnemy : HomePointEnemyModel
    {
        private static readonly int Attack = Animator.StringToHash("attack");
        private float _attackDelay = .4f;

        protected override void CheckDistance()
        {
            var distance = Vector3.Distance(Target.position, transform.position);
            // if (Vector3.Distance(StartPosition, transform.position) < .01f && distance > chaseRad)
            // {
            //     Anim.SetBool(Move, false);
            //     return;
            // }
            if (Vector3.Distance(StartPosition, transform.position) < .01f && distance > chaseRad)
                return;
            if (distance <= attackRad)
            {
                if (CompareState(ObjectState.Walk) && !CompareState(ObjectState.Stagger)) 
                    StartCoroutine(AttackIe());
            }
            if(CompareState(ObjectState.Attack)) return;
            if (distance > chaseRad) MoveEnemy(StartPosition);
            else if (distance <= chaseRad && distance > attackRad) MoveEnemy(Target.position);
            ChangeState(ObjectState.Walk);
            // Anim.SetBool(Move, true);
        }

        private IEnumerator AttackIe()
        {
            ChangeState(ObjectState.Attack);
            Anim.SetBool(Attack, true);
            yield return new WaitForSeconds(_attackDelay);
            ChangeState(ObjectState.Walk);
            Anim.SetBool(Attack, false);
        }

        /// <summary>
        /// I should overriding this method, cause this enemy have 4 animations for moving (left, right, up, down)
        /// </summary>
        /// <param name="direction"></param>
        protected override void ChangeAnimation(Vector2 direction)
        {
            if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
            {
                if (direction.x > 0) SetAnFloat(Vector2.right);
                else if (direction.x < 0) SetAnFloat(Vector2.left);
            }
            else if (Mathf.Abs(direction.x) < Mathf.Abs(direction.y))
            {
                if (direction.y > 0) SetAnFloat(Vector2.up);
                else if (direction.y < 0) SetAnFloat(Vector2.down);
            }
        }
    }
}