﻿using System.Collections;
using Core;
using UnityEngine;

namespace Mehanics
{
    public class BulletEnemyModel : HomePointEnemyModel
    {
        [SerializeField] private BulletMovement movementPrefab;
        [SerializeField] private float shootDelay;

        private bool _canFire;
        
        public override void Start()
        {
            base.Start();
            _canFire = true;
        }

        protected override void CheckDistance()
        {
            var distance = Vector3.Distance(Target.position, transform.position);
            if (distance > chaseRad || !_canFire) return;
            var position = transform.position;
            var temp = Target.transform.position - position;
            var newBullet = Instantiate(movementPrefab, position, Quaternion.identity);
            newBullet.Launch(temp);
            StartCoroutine(CanFireIe());
        }

        private IEnumerator CanFireIe()
        {
            _canFire = false;
            yield return new WaitForSeconds(shootDelay);
            _canFire = true;
        }

#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = GizmosData.EnemyGizmosColor;
            Gizmos.DrawWireSphere(transform.position, chaseRad);
        }
#endif
    }
}