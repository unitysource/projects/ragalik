﻿using System;
using System.Collections;
using Core;
using Mehanics.Objects.Systems;
using ScriptableObjects;
using UnityEngine;

namespace Mehanics
{
    public class EnemyModel : AliveObjects
    {
        [Header("Enemy Properties")]
        protected Vector2 StartPosition;
        [SerializeField] private new string name;
        [SerializeField] private float speed;
        protected float Speed => speed;
        private void Awake() => 
            StartPosition = transform.position;

        private void OnEnable()
        {
            transform.position = StartPosition;
            CurState = ObjectState.Idle;
        }
    }
}