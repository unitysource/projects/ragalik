﻿using System;
using Mehanics.Objects.Systems;
using UnityEngine;

namespace Mehanics
{
    public class HomePointEnemyModel : EnemyModel
    {
        [Header("Target - it's Player")]
        protected Transform Target;

        [SerializeField] protected float chaseRad;
        [SerializeField] protected float attackRad;

        // private Vector2 _homePos;
        protected Animator Anim;
        private Rigidbody2D _rb;
        private SpriteRenderer _spriteRenderer;
        protected static readonly int Move = Animator.StringToHash("move");
        private static readonly int MoveY = Animator.StringToHash("moveY");
        private static readonly int MoveX = Animator.StringToHash("moveX");

        public virtual void Start()
        {
            ChangeState(ObjectState.Idle);
            Target = GameObject.FindWithTag("Player").transform;
            _rb = GetComponent<Rigidbody2D>();
            Anim = GetComponent<Animator>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        

        private void FixedUpdate() => CheckDistance();

        protected virtual void CheckDistance()
        {
            var distance = Vector3.Distance(Target.position, transform.position);
            if (Vector3.Distance(StartPosition, transform.position) < .01f && distance > chaseRad
                || distance <= attackRad)
            {
                Anim.SetBool(Move, false);
                return;
            }

            if (CompareState(ObjectState.Stagger)) return;
            if (distance > chaseRad) MoveEnemy(StartPosition);
            else MoveEnemy(Target.position);
            ChangeState(ObjectState.Walk);
            Anim.SetBool(Move, true);
        }

        /// <summary>
        /// Move enemy, flip and animating(if enemy go up his animation go up the same)
        /// </summary>
        /// <param name="targetPos"> it is a position which this enemy should come (home or player) </param>
        protected void MoveEnemy(Vector2 targetPos)
        {
            var position = transform.position;
            var temp = Vector3.MoveTowards(position,
                targetPos, Speed * Time.deltaTime);
            var direction = temp - position;
            ChangeAnimation(direction);
            _rb.MovePosition(temp);
        }

        protected virtual void ChangeAnimation(Vector2 direction)
        {
            if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
            {
                if (direction.x > 0)
                {
                    SetAnFloat(Vector2.right);
                    _spriteRenderer.flipX = false;
                }
                else if (direction.x < 0)
                {
                    SetAnFloat(Vector2.left);
                    _spriteRenderer.flipX = true;
                }
            }
            else if (Mathf.Abs(direction.x) < Mathf.Abs(direction.y))
            {
                if (direction.y > 0) SetAnFloat(Vector2.up);
                else if (direction.y < 0) SetAnFloat(Vector2.down);
            }
        }
        protected void SetAnFloat(Vector2 normDir)
        {
            Anim.SetFloat(MoveX, normDir.x);
            Anim.SetFloat(MoveY, normDir.y);
        }
    }
}