﻿using Core;
using Mehanics.Objects.Systems;
using UnityEngine;

namespace Mehanics
{
    public class PatrolEnemyModel : HomePointEnemyModel
    {
        private byte _currPoint;
        private EdgeCollider2D _collider;
        private Vector2[] _points = new Vector2[2];

        public override void Start()
        {
#if UNITY_EDITOR
            _basedPos = transform.position;
#endif
            base.Start();
            _points = new Vector2[2];
            _collider = transform.parent.GetComponent<EdgeCollider2D>();
            for (var i = 0; i < _collider.points.Length; i++)
                _points[i] = _collider.points[i] + StartPosition;
        }

        protected override void CheckDistance()
        {
            var position = transform.position;
            var distance = Vector3.Distance(Target.position, position);
            if (distance <= attackRad)
            {
                Anim.SetBool(Move, false);
                return;
            }

            if (CompareState(ObjectState.Stagger)) return;
            if (distance > chaseRad)
            {
                if (Vector3.Distance(position, _points[_currPoint]) > .2f)
                    MoveEnemy(_points[_currPoint]);
                else ChangeGoal();
            }
            else MoveEnemy(Target.position);

            ChangeState(ObjectState.Walk);
            Anim.SetBool(Move, true);
        }

        private void ChangeGoal() =>
            _currPoint = (byte) (_currPoint == _collider.points.Length - 1 ? _currPoint - 1 : _currPoint + 1);

#if UNITY_EDITOR
        private Vector2? _basedPos;
        private void OnDrawGizmosSelected()
        {
            var pos = _basedPos ?? (Vector2) transform.position;
            _collider = transform.parent.GetComponent<EdgeCollider2D>();
            for (var i = 0; i < _collider.points.Length; i++)
                _points[i] = _collider.points[i] + pos;
            Gizmos.color = GizmosData.EnemyGizmosColor;
            Gizmos.DrawLine(_points[0], _points[1]);
        }
#endif
    }
}