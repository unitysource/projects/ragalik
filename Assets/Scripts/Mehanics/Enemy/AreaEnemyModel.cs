﻿using Core;
using Mehanics.Objects.Systems;
using UnityEngine;

namespace Mehanics
{
    public class AreaEnemyModel : HomePointEnemyModel
    {
        private Collider2D _areaBounds;
        
        public override void Start()
        {
            base.Start();
            _areaBounds = transform.parent.GetComponent<Collider2D>();
        }

        protected override void CheckDistance()
        {
            var distance = Vector3.Distance(Target.position, transform.position);
            if (distance <= attackRad ||
                !_areaBounds.bounds.Contains(Target.transform.position))
            {
                Anim.SetBool(Move, false);
                return;
            }

            if (CompareState(ObjectState.Stagger)) return;
            // if (distance > chaseRad) MoveEnemy(HomePos);
            MoveEnemy(Target.position);
            ChangeState(ObjectState.Walk);
            Anim.SetBool(Move, true);
        }
        
#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            Transform transform1;
            _areaBounds = (transform1 = transform).parent.GetComponent<Collider2D>();
            Gizmos.color = GizmosData.EnemyGizmosColor;
            Gizmos.DrawWireCube(transform1.position, _areaBounds.bounds.size);
        }
#endif
    }
}