﻿using System;
using Mehanics.Objects.Systems;
using ScriptableObjects;
using SuperTiled2Unity;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Mehanics
{
    public class ChangeFormAttack : MonoBehaviour
    {
        [SerializeField] private GameObject attackJoystick;
        [Header("Ближний бой")]
        [SerializeField] private Sprite meleeSprite;
        [Header("Дальний бой")]
        [SerializeField] private Sprite rangedSprite;
        [SerializeField] private WeaponSystem weaponSystem;

        private Image _joystickCircleImage;
        private Joystick _joystickClass;
        private Button _meleeAttackButton;

        private void Awake()
        {
            _joystickCircleImage = attackJoystick.GetComponent<Image>();
            _joystickClass = attackJoystick.GetComponent<Joystick>();
            _meleeAttackButton = attackJoystick.GetComponent<Button>();

            OnChangeWeapon(0);
        }

        public void OnChangeWeapon(int weaponId)
        {
            var newWeapon = weaponSystem.GetWeapon(weaponId);
            weaponSystem.SetWeapon(newWeapon);
            newWeapon.ApplyUi(_joystickCircleImage, _joystickClass, _meleeAttackButton, meleeSprite, rangedSprite);
            Debug.Log(newWeapon.Name);
        }
    }
}