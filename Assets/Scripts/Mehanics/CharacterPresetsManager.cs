﻿using System.Globalization;
using ScriptableObjects;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Mehanics
{
    public class CharacterPresetsManager : MonoBehaviour
    {
        [SerializeField] private Image healthBox;
        [SerializeField] private Image patronsBox;
        [SerializeField] private Image shieldBox;

        [SerializeField] private TextMeshProUGUI healthText;
        [SerializeField] private TextMeshProUGUI patronsText;
        [SerializeField] private TextMeshProUGUI shieldText;

        [SerializeField] private CharacterPresets characterPresets;

        // public CharacterPresets CharacterPresets => characterPresets;

        private void Start()
        {
            characterPresets.HealthPreset.ResetValue();
            characterPresets.PatronsPreset.ResetValue();
            characterPresets.ShieldPreset.ResetValue();

            UpdateHealth();
            UpdatePatrons();
            UpdateShield();
        }

        public void UpdateHealth()
        {
            characterPresets.HealthPreset.UpdateValue();
            healthText.text = characterPresets.HealthPreset.CurrentValue.ToString(CultureInfo.InvariantCulture);
            healthBox.fillAmount = characterPresets.HealthPreset.NormalizeValue;
        }

        public void UpdatePatrons()
        {
            characterPresets.PatronsPreset.UpdateValue();
            patronsText.text = characterPresets.PatronsPreset.CurrentValue.ToString(CultureInfo.InvariantCulture);
            patronsBox.fillAmount = characterPresets.PatronsPreset.NormalizeValue;
        }

        public void UpdateShield()
        {
            characterPresets.ShieldPreset.UpdateValue();
            shieldText.text = characterPresets.ShieldPreset.CurrentValue.ToString(CultureInfo.InvariantCulture);
            shieldBox.fillAmount = characterPresets.ShieldPreset.NormalizeValue;
        }
    }
}