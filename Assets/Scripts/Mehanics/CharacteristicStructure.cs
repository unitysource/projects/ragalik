﻿using UnityEngine;

namespace Mehanics
{
    [System.Serializable]
    public struct CharacteristicStructure
    {
        [SerializeField] private int minValue;
        [SerializeField] private int maxValue;

        public void GetRandomValue() =>
            Random.Range(minValue, maxValue);
    }
}