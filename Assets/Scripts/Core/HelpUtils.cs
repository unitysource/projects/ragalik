﻿using System.Collections;
using Mehanics;
using ScriptableObjects;
using UnityEngine;

namespace Core
{
    public static class HelpUtils
    {
        public static void SetReaction(int increaseAmount, FloatValue floatValue, Signal signal)
        {
            if (floatValue.RuntimeValue >= floatValue.InitialValue) return;
            floatValue.RuntimeValue += increaseAmount;
            if (floatValue.RuntimeValue > floatValue.InitialValue)
                floatValue.RuntimeValue = floatValue.InitialValue;
            signal.Raise();
        }
        
        // public static void DecreaseAmount(int decreaseAmount, int numberHeld)
        // {
        //     numberHeld -= decreaseAmount;
        //     if (numberHeld < 0)
        //         numberHeld = 0;
        // }
    }
}