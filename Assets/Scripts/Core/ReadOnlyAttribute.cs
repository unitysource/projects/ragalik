﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace Core
{
    public class ReadOnlyAttribute : PropertyAttribute
    {
    }
    
    [CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
    public class ReadOnlyAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            GUI.enabled = false;
            EditorGUI.PropertyField(position, property, label);
            GUI.enabled = true;
        }
    }
}
#endif