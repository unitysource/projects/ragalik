﻿using Mehanics;
using UnityEditor;
using UnityEngine;

namespace Core
{
    public class ToolsMenuItems
    {
#if UNITY_EDITOR
        [MenuItem("Tools/Clear PlayerPrefs")]
        private static void ClearPlayerPrefs()
        {
            Debug.Log("Player Prefs storage is clean like your ass");
            PlayerPrefs.DeleteAll();
        }

        [MenuItem("Tools/Save ScriptableObjects")]
        private static void SaveScriptableObjects()
        {
            Debug.Log("ScriptableObjects storage was save");
            SaveGameManager.SaveObjectsStatic(SaveGameManager.Objects);
        }

        // [MenuItem("Tools/Clear ScriptableObjects")]
        // private static void ClearScriptableObjects()
        // {
        //     Debug.Log("ScriptableObjects storage is clean like your ass");
        //     
        // }
#endif
    }
}