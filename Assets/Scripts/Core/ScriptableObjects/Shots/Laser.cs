﻿using UnityEngine;
using Zenject.Asteroids;

namespace ScriptableObjects.Shots
{
    [CreateAssetMenu(fileName = "NEW LASER", menuName = "SCRIPTABLEOBJECTS/SHOTS/LASER")]
    public class Laser : Shot
    {
        [SerializeField] private Color laserRayColor;
        [SerializeField] private float oneSecondShootCost;
        
        public override void InitShot()
        {
            throw new System.NotImplementedException();
        }
    }
}