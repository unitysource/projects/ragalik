﻿using System.Collections;
using UnityEngine;

namespace ScriptableObjects.Shots
{
    // [CreateAssetMenu(fileName = "NEW SHOT", menuName = "SCRIPTABLEOBJECTS/SHOTS/SHOT")]
    public abstract class Shot : ScriptableObject
    {
        [SerializeField] private float knockTime;
        [SerializeField] private float damage;
        [SerializeField] private float thrust;
        
        public float KnockTime => knockTime;

        public float Damage => damage;

        public float Thrust => thrust;
        
        public abstract void InitShot();
    }
}