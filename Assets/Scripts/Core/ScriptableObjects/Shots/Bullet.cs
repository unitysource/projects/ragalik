﻿using System.Collections;
using UnityEngine;

namespace ScriptableObjects.Shots
{
    [CreateAssetMenu(fileName = "NEW BULLET", menuName = "SCRIPTABLEOBJECTS/SHOTS/BULLET")]
    public class Bullet : Shot
    {
        [SerializeField] private float lifeTime;
        [SerializeField] private float speed;
        [SerializeField] private float oneShootCost;
        [SerializeField] private Sprite bulletSprite;
        
        public override void InitShot()
        {
            
        }
    }
}