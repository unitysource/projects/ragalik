﻿using Core;
using UnityEngine;

namespace ScriptableObjects
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "New FloatValue", menuName = "SCRIPTABLEOBJECTS/VALUES/FLOAT VALUE", order = 0)]
    public class FloatValue : ScriptableObject
    {
        [SerializeField] private float initialValue;
        [SerializeField] [ReadOnly] private float runtimeValue;

        public float InitialValue
        {
            get => initialValue;
            set => initialValue = value;
        }

        public float RuntimeValue
        {
            get => runtimeValue;
            set => runtimeValue = value;
        }
    }
}