﻿using UnityEngine;

namespace ScriptableObjects
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "NEW BoolValue", menuName = "SCRIPTABLEOBJECTS/VALUES/BOOL VALUE")]
    public class BoolValue : ScriptableObject
    {
        [SerializeField] private bool initialValue;
        public bool InitialValue => initialValue;

        public bool RuntimeValue { get; set; }

        public void OnBeforeSerialize() { }

        public void OnAfterDeserialize() =>
            RuntimeValue = initialValue;
    }
}