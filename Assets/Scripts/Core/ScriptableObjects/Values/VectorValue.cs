﻿using UnityEngine;

namespace ScriptableObjects
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "New Vector Value", menuName = "SCRIPTABLEOBJECTS/VALUES/VECTOR VALUE", order = 2)]
    public class VectorValue : ScriptableObject
    {
        [Tooltip("Changing in game")]
        [SerializeField] private Vector2 initialValue;
        [Tooltip("Default position of player. If game ending in another scene player spawn in this position")]
        [SerializeField] private Vector2 homesteadValue;

        public Vector2 InitialValue
        {
            get => initialValue;
            set => initialValue = value;
        }
    }
}