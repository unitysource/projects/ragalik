﻿using Mehanics;
using UnityEngine;

namespace ScriptableObjects
{
    [System.Serializable]
    public class Loot
    {
        [SerializeField] private PowerUp powerUp;
        public PowerUp PowerUp => powerUp;
        [SerializeField] private int lootChance;
        public int LootChance => lootChance;
    }

    [CreateAssetMenu(fileName = "New Loot Table", menuName = "SCRIPTABLEOBJECTS/LOOT TABLE")]
    public class LootTable : ScriptableObject
    {
        [SerializeField] private Loot[] loots;

        public PowerUp LootPowerUp()
        {
            int temp = 0;
            int rand = Random.Range(0, 100);
            foreach (var loot in loots)
            {
                temp += loot.LootChance;
                if (temp >= rand) return loot.PowerUp;
            }
            return null;
        }
    }
}