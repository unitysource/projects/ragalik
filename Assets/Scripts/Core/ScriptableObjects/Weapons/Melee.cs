﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = "NEW MELEE", menuName = "SCRIPTABLEOBJECTS/WEAPONS/MELEE")]
    public class Melee : Weapon
    {
        // static Melee() => WeaponType = WeaponType.Melee;

        public override void WeaponAttack()
        {
        }

        public override void WeaponDamage(Rigidbody2D targetRb, Vector3 ownPosition, float damageModifier = 1)
        {
        }

        public override void ApplyUi(Image joystickCircleImage, Joystick joystickClass,
            Button meleeAttackButton, Sprite meleeSprite, Sprite rangedSprite)
        {
            joystickCircleImage.sprite = meleeSprite;
            joystickClass.enabled = false;
            meleeAttackButton.enabled = true;
        }
    }
}