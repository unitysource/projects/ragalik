﻿using System.Collections;
using System.Diagnostics.Contracts;
using DG.Tweening;
using Mehanics;
using Mehanics.Objects.Systems;
using UnityEngine;
using UnityEngine.UI;

namespace ScriptableObjects
{
    public enum WeaponType
    {
        Melee,
        Ranged
    }

    public abstract class Weapon : ScriptableObject
    {
        [SerializeField] private new string name;
        [SerializeField] private string description;
        [SerializeField] private Sprite handsSprite;
        [SerializeField] private GameObject prefabInScene;
        [SerializeField] private float knockTime;
        [SerializeField] private float damage;
        [SerializeField] private float thrust;
        [SerializeField] private float attackDelay;
        [SerializeField] private WeaponType weaponType;

        // public static WeaponType WeaponType => 

        protected bool CanAttack = true;
        public string Name => name;
        public bool IsBroken { get; protected set; }
        public bool IsReadyToAttack { get; protected set; }

        /// <summary>
        /// Some delay after attack
        /// </summary>
        /// <returns></returns>
        protected IEnumerator AttackCooldown()
        {
            CanAttack = false;
            yield return new WaitForSeconds(attackDelay);
            CanAttack = true;
        }

        /// <summary>
        /// When we triggered Attack Joystick
        /// </summary>
        public abstract void WeaponAttack();

        /// <summary>
        /// When ownTransform bite target
        /// </summary>
        /// <param name="targetRb"></param>
        /// <param name="ownTransform"></param>
        /// <param name="damageModifier"></param>
        public abstract void WeaponDamage(Rigidbody2D targetRb, Vector3 ownPosition, float damageModifier = 1);

        protected void AddImpulse(Rigidbody2D targetRb, Vector3 ownPosition)
        {
            if (targetRb is null) return;
            var targetPosition = targetRb.transform.position;
            var diff = (targetPosition - ownPosition).normalized * thrust;
            targetRb.DOMove(targetPosition + diff, knockTime);
        }

        protected virtual void ApplyDamage(Rigidbody2D targetRb)
        {
            // AddImpulse(targetRb);
            var targetObject = targetRb.GetComponent<AliveObjects>();
            var targetHealth = targetRb.GetComponent<HealthSystem>();
            if (targetObject.CompareState(ObjectState.Stagger)) return;
            targetObject.ChangeState(ObjectState.Stagger);
            targetHealth.Knock(targetRb, knockTime);
            targetHealth.Damage(-damage);
        }

        public abstract void ApplyUi(Image joystickCircleImage, Joystick joystickClass, Button meleeAttackButton, Sprite meleeSprite, Sprite rangedSprite);
    }
}