﻿using UnityEngine;
using UnityEngine.UI;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = "New Ranged", menuName = "SCRIPTABLEOBJECTS/WEAPONS/Ranged", order = 0)]
    public class Ranged : Weapon
    {
        
        public override void WeaponAttack()
        {
        }

        public override void WeaponDamage(Rigidbody2D targetRb, Vector3 ownPosition, float damageModifier = 1)
        {
        }

        public override void ApplyUi(Image joystickCircleImage, Joystick joystickClass, 
            Button meleeAttackButton, Sprite meleeSprite, Sprite rangedSprite)
        {
            joystickCircleImage.sprite = rangedSprite;
            joystickClass.enabled = true;
            meleeAttackButton.enabled = false;
        }
    }
}