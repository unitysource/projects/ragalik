﻿using DG.Tweening;
using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = "NEW DASH ABILITY", menuName = "SCRIPTABLEOBJECTS/ABILITIES/DASH ABILITY")]
    public class DashAbility : AbilitySystemObject
    {
        [SerializeField] private float dashForce;

        public override void Ability(Vector2 playerPosition, Vector2 playerFacingDir, Animator plAnim = null, Rigidbody2D playerRb = null)
        {
            //Is player have enought magic
            if (PlayerMagic.RuntimeValue >= MagicCost)
            {
                PlayerMagic.RuntimeValue -= MagicCost;
                UsePlayerMagic.Raise();
            } else return;

            if (playerRb == null) return;
            var dashVectorTemp = playerRb.transform.position + (Vector3) playerFacingDir.normalized * dashForce;
            playerRb.DOMove(dashVectorTemp, Duration);
        }
    }
}