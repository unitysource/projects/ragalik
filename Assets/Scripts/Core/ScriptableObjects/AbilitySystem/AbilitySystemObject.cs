﻿using UnityEngine;
using UnityEngine.Playables;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = "NEW ABILITY", menuName = "SCRIPTABLEOBJECTS/ABILITIES/ABILITY")]
    public class AbilitySystemObject : ScriptableObject
    {
        [SerializeField] private float magicCost;

        public float MagicCost => magicCost;
        [SerializeField] private float duration;

        public float Duration => duration;

        [SerializeField] private FloatValue playerMagic;

        public FloatValue PlayerMagic => playerMagic;
        [SerializeField] private Signal usePlayerMagic;

        public Signal UsePlayerMagic => usePlayerMagic;
        
        public virtual void Ability(Vector2 playerPosition, Vector2 playerFacingDir, Animator plAnim = null, Rigidbody2D playerRb = null)
        {
            
        }
    }
}