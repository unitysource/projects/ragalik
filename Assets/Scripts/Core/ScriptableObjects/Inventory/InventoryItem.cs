﻿using UnityEngine;
using UnityEngine.Events;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = "New Item", menuName = "SCRIPTABLEOBJECTS/PLAYER INVENTORY/INVENTORY ITEM")]
    public class InventoryItem : ScriptableObject
    {
        [SerializeField] private string itemName;

        public string ItemName => itemName;
        [SerializeField] private string itemDescription;

        public string ItemDescription => itemDescription;
        [SerializeField] private Sprite itemSprite;

        public Sprite ItemSprite => itemSprite;
        [SerializeField] private int numberHeld;

        public int NumberHeld
        {
            get => numberHeld;
            set => numberHeld = value;
        }

        [SerializeField] private bool usable;

        public bool Usable => usable;
        [SerializeField] private bool unique;

        public bool Unique => unique;
        
        [SerializeField] private UnityEvent thisEvent;

        public void UseToggle()
        {
            thisEvent?.Invoke();
            Debug.Log($"using {itemName}");
        }

        /// <summary>
        /// It's used in InventoryItems. Decrease amount of something
        /// </summary>
        /// <param name="decreaseAmount"></param>
        public void DecreaseAmount(int decreaseAmount)
        {
            numberHeld -= decreaseAmount;
            if (numberHeld < 0)
                numberHeld = 0;
        }
    }
}