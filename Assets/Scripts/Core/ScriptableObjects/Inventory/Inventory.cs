﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableObjects
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "New Inventory", menuName = "SCRIPTABLEOBJECTS/INVENTORY/INVENTORY")]
    public class Inventory : ScriptableObject
    {
        public Item CurrItem { get; set; }

        // private readonly Queue<Item> _items = new Queue<Item>();
        private List<Item> _items = new List<Item>();

        public int KeysNumber
        {
            get => keysNumber;
            set => keysNumber = value;
        }

        public int CoinsNumber
        {
            get => coinsNumber;
            set => coinsNumber = value;
        }

        [SerializeField] private int keysNumber;
        [SerializeField] private int coinsNumber;

        public void AddItem(Item itemToAdd)
        {
            if (itemToAdd.IsKey)
                KeysNumber++;
            else if (!_items.Contains(itemToAdd))
                _items.Add(itemToAdd);
                // _items.Enqueue(itemToAdd);
        }

        public bool CheckItem(Item item) => _items.Contains(item);
    }
}