﻿using UnityEngine;

namespace ScriptableObjects
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "New Item", menuName = "SCRIPTABLEOBJECTS/INVENTORY/ITEM")]
    public class Item : ScriptableObject
    {
        [SerializeField] private Sprite itemSprite;
        public Sprite ItemSprite => itemSprite;
        [SerializeField] private string itemDescription;
        public string ItemDescription => itemDescription;
        [SerializeField] private bool isKey;
        public bool IsKey => isKey;
        
    }
}