﻿using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = "New Manager", menuName = "SCRIPTABLEOBJECTS/Float Value Manager")]
    public class FloatValueManager : ScriptableObject
    {
        private float _value;
        [SerializeField] private float resetValue;
        
        
    }
}