﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = "New Signal", menuName = "SCRIPTABLEOBJECTS/SIGNAL", order = 1)]
    public class Signal : ScriptableObject
    {
        // HashSet may crash all)))
        private readonly HashSet<SignalListener> _listeners = new HashSet<SignalListener>();

        public void Raise()
        {
            foreach (var listener in _listeners) 
                listener.OnSignalRaise();
        }

        public void AddListener(SignalListener listener) => _listeners.Add(listener);

        public void RemoveListener(SignalListener listener) => _listeners.Remove(listener);
    }
}