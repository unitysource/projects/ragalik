﻿namespace Core
{
    public static class Tags
    {
        public const string Player = "Player";
        public const string Enemy = "enemy";
        public const string BreakableObject = "breakableObject";
    }
}