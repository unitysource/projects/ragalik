﻿using UnityEngine;

namespace Core
{
    public static class MonoBehaviourExtensions
    {
        public static Transform GetWeapon(this MonoBehaviour mb) => mb.transform.Find("Weapon");
    }
}