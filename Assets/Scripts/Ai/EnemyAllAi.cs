using Pathfinding;
using UnityEngine;

namespace Ai
{
    public class EnemyAllAi : MonoBehaviour
    {
        [SerializeField] private Transform target;

        [SerializeField] private float speed = 200f;
        [SerializeField] private float nextWayDistance = 3f;
        [SerializeField] private float updateRepeating = 2f;
        [SerializeField] private Transform enemyGFX;

        private Path _path;
        private int _curWayPoint = 0;
        private bool reachedEndOfPath = false;

        private Seeker _seeker;
        private Rigidbody2D _riba;
    
        private void Start()
        {
            _seeker = GetComponent<Seeker>();
            _riba = GetComponent<Rigidbody2D>();

            InvokeRepeating("UpdatePath", 0f, updateRepeating);
        }

        private void UpdatePath()
        {
            if(_seeker.IsDone()) 
                _seeker.StartPath(_riba.position, target.position, OnPathConfirm);
        }

        private void OnPathConfirm(Path path)
        {
            if (path.error) return;
            _path = path;
            _curWayPoint = 0;
        }

        private void FixedUpdate()
        {
            if(_path is null) return;
            if (_curWayPoint >= _path.vectorPath.Count)
            {
                reachedEndOfPath = true;
                return;
            }
            else reachedEndOfPath = false;

            var position = _riba.position;
            var dir = ((Vector2)_path.vectorPath[_curWayPoint] - position).normalized;
            var force = dir * speed * Time.deltaTime;
        
            _riba.AddForce(force);

            var distance = Vector2.Distance(position, _path.vectorPath[_curWayPoint]);

            if (distance < nextWayDistance) _curWayPoint++;
        
            if (force.x >= 0.01f)
                enemyGFX.localScale = new Vector3(-1, 1, 1);
            else if (force.x <= -.01f) 
                enemyGFX.localScale = new Vector3(1, 1, 1);
        }
    }
}
