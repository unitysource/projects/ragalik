using UnityEngine;

namespace Ai
{
    public class EnemyAi : MonoBehaviour
    {
        [SerializeField] private float speed = 1;
        [SerializeField] private float stopDistance = 1;

        private Transform _target;

        void Start()
        {
            _target = GameObject.FindWithTag("Player").transform;
        }

        void Update()
        {
            if (_target is null) return;

            if(Vector2.Distance(transform.position, _target.position) < stopDistance)
                transform.position = Vector2.MoveTowards(transform.position, _target.transform.position, speed * Time.deltaTime);
        }
    }
}