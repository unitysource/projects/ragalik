# Ragalik

## Table of Contents
- [About the Game](#about-the-game)
- [Gameplay](#gameplay)
- [Features](#features)
- [Getting Started](#getting-started)
- [Installation](#installation)
- [Controls](#controls)
- [Contributing](#contributing)

## About the Game

Ragalik genre gameplay

## Gameplay

Solve the puzzle, find the way out of the maze, and escape the dungeon.

![Gameplay GIF](/images/gameplay.gif)

## Features

Key features of game:
- 2D platformer
- 2D physics
- 2D animation

## Getting Started

To download the game, go to the [releases page]

### Installation

1. Clone the repository: `git clone `
2. Navigate to the game directory: `cd bear`
3. Install dependencies: `npm install` or `yarn install`

### Controls

Only mobile controls are available at the moment.

**Mobile Controls:**
- Swipe left/right to move.
- Tap to jump.

## Acknowledgments

- [Sound effects by SoundBible](https://www.soundbible.com/)
